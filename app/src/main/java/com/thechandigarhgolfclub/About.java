package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.ShowMsg;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.Utils.SlideshowDialogFragment;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class About extends Activity implements Iconstant, ConnectivityReceiveListener {
    private static final long RIPPLE_DURATION = 250;
    ProgressDialog progressDialog;
    RelativeLayout toolbar;
    FrameLayout root;
    BadgeView badgeView;
    ImageView image_about;
    View contentHamburger;
    TextView scorecard, clubrule, dresscode;
    View view_scorecard, view_clubrule, view_dresscode;
    GalleryAdapter galleryAdapter;
    // ArrayList<String> mArrayImage;
    private TextView title;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout, notification;
    //  private FirebaseAnalytics mFirebaseAnalytics;
    private SharedPreferences sharedPreferences1;
    //  ListView listImage;
    private RecyclerView recyclerView;
    private ArrayList<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scorecard);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        //progressDialog = new ProgressDialog(About.this);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        scorecard = (TextView) findViewById(R.id.scorecard);
        clubrule = (TextView) findViewById(R.id.clubrule);
        title = (TextView) findViewById(R.id.title);
        dresscode = (TextView) findViewById(R.id.dresscode);
        view_scorecard = (View) findViewById(R.id.view_scorecard);
        view_clubrule = (View) findViewById(R.id.view_clubrule);
        view_dresscode = (View) findViewById(R.id.view_dresscode);
        logout = (ImageView) findViewById(R.id.logout);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        image_about = (ImageView) findViewById(R.id.image_about);

        title.setText(getResources().getString(R.string.scorecard));
        images = new ArrayList<>();
        // RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(About.this);
        editor = sharedPreference.edit();
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppController.connection) {
                    Singleton.getinstance().logout(About.this, editor);

                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }

            }
        });

        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(About.this, notification);
        //noti_count();

        if (AppController.connection) {
            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
            Singleton.getinstance().noti_count(About.this, badgeView);

        } else {
            // findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
            //  closeProgressDialog();
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }

        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, Notification_list.class);
                intent.putExtra("back_noti", About.class);
                startActivity(intent);
                //   finish();
            }
        });

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(About.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

        if (AppController.connection) {
            json_about("Scorecard", getResources().getString(R.string.scorecard));
        } else {
            findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
            // closeProgressDialog();
            showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
        }

        view_scorecard.setBackgroundColor(getResources().getColor(R.color.btn_text));
        view_clubrule.setBackgroundColor(getResources().getColor(R.color.appbar));
        view_dresscode.setBackgroundColor(getResources().getColor(R.color.appbar));

        scorecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.connection) {
                    json_about("Scorecard", getResources().getString(R.string.scorecard));
                } else {
                    findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
                    // closeProgressDialog();
                    showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
                }
                view_scorecard.setBackgroundColor(getResources().getColor(R.color.btn_text));
                view_clubrule.setBackgroundColor(getResources().getColor(R.color.appbar));
                view_dresscode.setBackgroundColor(getResources().getColor(R.color.appbar));
            }
        });

        clubrule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.connection) {
                    json_about("Clubrule", getResources().getString(R.string.clubrule));
                } else {
                    findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
                    // closeProgressDialog();
                    showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
                }
                view_scorecard.setBackgroundColor(getResources().getColor(R.color.appbar));
                view_clubrule.setBackgroundColor(getResources().getColor(R.color.btn_text));
                view_dresscode.setBackgroundColor(getResources().getColor(R.color.appbar));
            }
        });

        dresscode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.connection) {
                    json_about("Dresscode", getResources().getString(R.string.dresscode));
                } else {
                    findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
                    // closeProgressDialog();
                    showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
                }
                view_scorecard.setBackgroundColor(getResources().getColor(R.color.appbar));
                view_clubrule.setBackgroundColor(getResources().getColor(R.color.appbar));
                view_dresscode.setBackgroundColor(getResources().getColor(R.color.btn_text));
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    // json of about
    void json_about(final String type, final String type_nofound) {
        images = new ArrayList<>();
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        //   mArrayImage = new ArrayList<>();
        showProgressDialog("Loading " + type);
        // String url = Baseurl + "home.php?page=aboutus";
        String url = Baseurl + "scoreboard.php?type=" + type;

        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String plainText = Html.fromHtml(response).toString();
                try {
                    JSONObject jsonObject = new JSONObject(plainText);
                    String status = jsonObject.getString("status");
                    Log.d("abouturlstatus", status);
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Log.d("jsonArray", String.valueOf(jsonArray));
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            Image image = new Image();
                            image.setMedium(jsonObject1.getString("data").toString());
                            images.add(image);
                        }
                        galleryAdapter = new GalleryAdapter(getApplicationContext(), images);
                        recyclerView.setAdapter(galleryAdapter);

                    } else {
                        galleryAdapter = new GalleryAdapter(getApplicationContext(), images);
                        recyclerView.setAdapter(galleryAdapter);
                        new ShowMsg().createDialog(About.this, "No " + type_nofound + " Found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                closeProgressDialog();
                Log.d("error", error.toString());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }


    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            // json_about();
            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    class ImageAdapter extends BaseAdapter {
        ImageLoader imageLoader;
        ArrayList<String> mImageList;
        DisplayImageOptions options;

        ImageAdapter(ArrayList<String> mImageList) {
            this.mImageList = mImageList;
        }

        @Override
        public int getCount() {
            return mImageList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = LayoutInflater.from(About.this);
            view = layoutInflater.inflate(R.layout.about_galleryrow, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(About.this));
            options = new DisplayImageOptions.Builder().cacheOnDisc().cacheInMemory().build();
            // imageLoader.displayImage(mImageList.get(i), imageView, options);

            imageLoader.displayImage(mImageList.get(i), imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {

                    //pbHeaderProgress.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(FailReason failReason) {
                    closeProgressDialog();
                    // pbHeaderProgress.setVisibility(View.GONE);
                }//

                @Override
                public void onLoadingComplete(Bitmap bitmap) {
                    closeProgressDialog();
                    //closeProgressDialog();
                    //   pbHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled() {

                    //pbHeaderProgress.setVisibility(View.GONE);
                }
            });
            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final Dialog settingsDialog = new Dialog(About.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.gg
                            , null));
                    ImageView imageView1 = (ImageView) settingsDialog.findViewById(R.id.img);
                    ImageView back_btn = (ImageView) settingsDialog.findViewById(R.id.back_btn);
                    ScaleAnimation anim = new ScaleAnimation(1f, 1f, 1f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    anim.setDuration(500);
                    anim.setFillAfter(true);
                    imageView1.startAnimation(anim);
                    imageLoader.displayImage(mImageList.get(i), imageView1, options);
                    settingsDialog.show();

                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            settingsDialog.dismiss();
                        }
                    });
                }
            });
            return view;
        }
    }
}