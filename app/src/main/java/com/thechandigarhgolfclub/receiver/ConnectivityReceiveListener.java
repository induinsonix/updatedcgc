package com.thechandigarhgolfclub.receiver;

public interface ConnectivityReceiveListener {
    void onNetworkConnectionChanged(boolean isConnected);
}
