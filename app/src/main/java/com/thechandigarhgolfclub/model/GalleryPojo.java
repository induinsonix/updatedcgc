
package com.thechandigarhgolfclub.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GalleryPojo {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("image")
    @Expose
    private List<String> image = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

}
