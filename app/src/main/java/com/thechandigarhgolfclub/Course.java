package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter;
import com.thechandigarhgolfclub.Utils.GalleryAdapter1;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.Utils.SlideshowDialogFragment;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.CoursePojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;


public class Course extends Activity implements Iconstant, ConnectivityReceiveListener {

    private static final long RIPPLE_DURATION = 250;

    RelativeLayout toolbar;

    FrameLayout root;
    TextView title;
    WebView text_course;
    View contentHamburger;
    // GridView gridView;
    private ImageView expandedImageView;
    int expanded_chek = 0;
    private ProgressDialog progressDialog;
    ArrayList image_list;
    private ImageView icon_app;
    ScrollView scrollview;
    private ImageView logout;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView notification;
    private BadgeView badgeView;
    private RecyclerView recyclerView;
    private ArrayList<Image> images;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Course Activity")
                .build());
        //Singleton.getinstance().saveValue(getApplicationContext(), "course_data","");
        gson = new Gson();
        //  expandedImageView = (ImageView)findViewById(R.id.expanded_image);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        text_course = (WebView) findViewById(R.id.text_course);
        // progressDialog=new ProgressDialog(Course.this);
        scrollview = (ScrollView) findViewById(R.id.scrollview);
        //  gridView = (GridView) findViewById(R.id.gridview);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //  gridView.setFocusable(false);
        images = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        image_list = new ArrayList();
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Course.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Course.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }

            }
        });
        title.setText("Course");

        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Course.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Course.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }


        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, Notification_list.class);
                startActivity(intent);
                // finish();
            }
        });

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);

        icon_app = (ImageView) findViewById(R.id.icon_app);

        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "course_data"))) {
            json_course(true);
        } else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "course_data");
            Log.e("plain", object);
            CoursePojo mResponseObject1 = gson.fromJson(object, CoursePojo.class);
            setData(mResponseObject1);
            json_course(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }


        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Course.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });


        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }


    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    @Override
    public void onBackPressed() {
//        Intent intent=new Intent(Course.this,CCBActivity.class);
//        startActivity(intent);
        finish();
    }


    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            json_course(false);
            Singleton.getinstance().noti_count(Course.this, badgeView);

            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }


    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    // json of course
    void json_course(boolean b) {

        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        if (b) {
            showProgressDialog(getString(R.string.course_loading));
        }

        String url = Baseurl + "course.php";
        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                Log.e("res", response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text", plainText);
                Singleton.getinstance().saveValue(getApplicationContext(), "course_data", plainText);
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(), "course_data"));
                CoursePojo userinfo1 = new Gson().fromJson(plainText, CoursePojo.class);
                setData(userinfo1);
                Log.e("data", userinfo1.toString());
                Log.e("data", userinfo1.getStatus().toString());
                closeProgressDialog();

              /*  String object = Singleton.getinstance().getValue(getApplicationContext(), "course_data").trim();
                JsonReader reader = new JsonReader(new StringReader(object));
                reader.setLenient(true);
                Type type = new TypeToken<CoursePojo>() {
                }.getType();
                CoursePojo mResponseObject = gson.fromJson(response, type);
                setData(mResponseObject);*/
               /* try {
                    jsonObject = new JSONObject(plainText);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setData(jsonObject);*/
                // progressDialog.dismiss();
                // closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.getMessage());
                String object = Singleton.getinstance().getValue(getApplicationContext(), "course_data");
                Log.e("plain", object);
                CoursePojo mResponseObject = gson.fromJson(object, CoursePojo.class);
                setData(mResponseObject);
                closeProgressDialog();

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void setData(CoursePojo mResponseObject) {
        images.clear();

        try {
            Log.e("mresponse", mResponseObject.toString());
            String status = mResponseObject.getStatus();
            if (status.equalsIgnoreCase("true")) {
                String data = String.valueOf(Html.fromHtml(mResponseObject.getData()));

                for (int i = 0; i < mResponseObject.getImage().size(); i++) {

                    Image image = new Image();
                    image.setMedium(mResponseObject.getImage().get(i));
                    images.add(image);
                }
                // mAdapter = new GalleryAdapter(getApplicationContext(), images);
                recyclerView.setAdapter(new GalleryAdapter1(getApplicationContext(), images));
                Spannable sp = new SpannableString(Html.fromHtml(mResponseObject.getData()));
                String html = "<html><body style=text-align:justify>" + Html.toHtml(sp) + "</body></html>";
                text_course.setBackgroundColor(getResources().getColor(R.color.background_textview));
                text_course.getSettings().setJavaScriptEnabled(true);
                text_course.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
                //  gridView.setAdapter(new ImageAdapter(Course.this, image_list));
                scrollview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
                scrollview.setFocusable(true);
            } else {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "course_data");
                Log.e("plain", object);
                CoursePojo mResponseObject1 = gson.fromJson(object, CoursePojo.class);
                setData(mResponseObject1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }
}

  /*  public class ImageAdapter extends BaseAdapter {
        ImageLoader imageLoader;
        private Animator mCurrentAnimator;
        DisplayImageOptions options;
        private int mShortAnimationDuration = 1000;

        private Context mContext;
        final int IDLE = 0;
        final int TOUCH = 1;
        final int PINCH = 2;
        float dist0, distCurrent;
        ArrayList<String> image_list;
        private Bitmap myBitmap;
        private int touchState;
        private ScaleGestureDetector SGD;
        private Matrix matrix = new Matrix();
        private float scale = 1f;
        private final ImageView.ScaleType[] scaleTypes = {ImageView.ScaleType.CENTER, ImageView.ScaleType.CENTER_CROP, ImageView.ScaleType.CENTER_INSIDE, ImageView.ScaleType.FIT_XY, ImageView.ScaleType.FIT_CENTER};
        private int index = 0;

        public ImageAdapter(Context c, ArrayList image_list) {
            mContext = c;
            this.image_list = image_list;
        }

        public int getCount() {
            return image_list.size();
        }

        public Object getItem(int position) {
            return image_list.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            int a = R.drawable.clubs;
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.row_gallery, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.image);

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
            imageLoader.displayImage(image_list.get(position), imageView, options);


            imageView.setTag(image_list.get(position));

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    int a = R.drawable.clubs;
                    final Dialog settingsDialog = new Dialog(Course.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.gg
                            , null));
                    final ImageView imageView1 = (ImageView) settingsDialog.findViewById(R.id.img);
                    ImageView back_btn = (ImageView) settingsDialog.findViewById(R.id.back_btn);

                    imageLoader.displayImage(image_list.get(position), imageView1, options);
//                    imageView1.setOnClickListener(new View.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            index = ++index % scaleTypes.length;
//                            ImageView.ScaleType currScaleType = scaleTypes[index];
//                            imageView1.setScaleType(currScaleType);
//                            //Toast.makeText(activity, "ScaleType: " + currScaleType, Toast.LENGTH_SHORT).show();
//                        }
//                    });


                    settingsDialog.show();
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            settingsDialog.dismiss();
                        }
                    });


                }
            });

            return convertView;
        }


        // References to our images in res > drawable
        public int getResourseId(Context context, String pVariableName, String pResourcename, String pPackageName) throws RuntimeException {
            try {
                return context.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
            } catch (Exception e) {
                throw new RuntimeException("Error getting Resource ID.", e);
            }
        }

        private void zoomImageFromThumb(final View thumbView, int imageResId) {
            expanded_chek = 1;
            // If there's an animation in progress, cancel it immediately and proceed with this one.
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }
            // Load the high-resolution "zoomed-in" image.

            // Load the high-resolution "zoomed-in" image.

            expandedImageView.setImageResource(imageResId);

            // Calculate the starting and ending bounds for the zoomed-in image. This step
            // involves lots of math. Yay, math.
            final Rect startBounds = new Rect();
            final Rect finalBounds = new Rect();
            final Point globalOffset = new Point();

            // The start bounds are the global visible rectangle of the thumbnail, and the
            // final bounds are the global visible rectangle of the container view. Also
            // set the container view's offset as the origin for the bounds, since that's
            // the origin for the positioning animation properties (X, Y).
            thumbView.getGlobalVisibleRect(startBounds);
            findViewById(R.id.container).getGlobalVisibleRect(finalBounds, globalOffset);
            startBounds.offset(-globalOffset.x, -globalOffset.y);
            finalBounds.offset(-globalOffset.x, -globalOffset.y);

            // Adjust the start bounds to be the same aspect ratio as the final bounds using the
            // "center crop" technique. This prevents undesirable stretching during the animation.
            // Also calculate the start scaling factor (the end scaling factor is always 1.0).
            float startScale;
            if ((float) finalBounds.width() / finalBounds.height()
                    > (float) startBounds.width() / startBounds.height()) {
                // Extend start bounds horizontally
                startScale = (float) startBounds.height() / finalBounds.height();
                float startWidth = startScale * finalBounds.width();
                float deltaWidth = (startWidth - startBounds.width()) / 2;
                startBounds.left -= deltaWidth;
                startBounds.right += deltaWidth;
            } else {
                // Extend start bounds vertically
                startScale = (float) startBounds.width() / finalBounds.width();
                float startHeight = startScale * finalBounds.height();
                float deltaHeight = (startHeight - startBounds.height()) / 2;
                startBounds.top -= deltaHeight;
                startBounds.bottom += deltaHeight;
            }


            thumbView.setAlpha(0f);
            expandedImageView.setVisibility(View.VISIBLE);

            expandedImageView.setPivotX(0f);
            expandedImageView.setPivotY(0f);

            AnimatorSet set = new AnimatorSet();
            set
                    .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left,
                            finalBounds.left))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top,
                            finalBounds.top))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale, 1f))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f));
            set.setDuration(mShortAnimationDuration);
            set.setInterpolator(new DecelerateInterpolator());
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mCurrentAnimator = null;
                }
            });
            set.start();
            mCurrentAnimator = set;

            // Upon clicking the zoomed-in image, it should zoom back down to the original bounds
            // and show the thumbnail instead of the expanded image.
            final float startScaleFinal = startScale;
            expandedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCurrentAnimator != null) {
                        mCurrentAnimator.cancel();
                    }
                    expanded_chek = 0;
                    // Animate the four positioning/sizing properties in parallel, back to their
                    // original values.
                    AnimatorSet set = new AnimatorSet();
                    set
                            .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left))
                            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, View.SCALE_X, startScaleFinal))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, View.SCALE_Y, startScaleFinal));
                    set.setDuration(mShortAnimationDuration);
                    set.setInterpolator(new DecelerateInterpolator());
                    set.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            thumbView.setAlpha(1f);
                            expandedImageView.setVisibility(View.GONE);
                            mCurrentAnimator = null;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            thumbView.setAlpha(1f);
                            expandedImageView.setVisibility(View.GONE);
                            mCurrentAnimator = null;
                        }
                    });
                    set.start();
                    mCurrentAnimator = set;
                }
            });
        }

    }*/



