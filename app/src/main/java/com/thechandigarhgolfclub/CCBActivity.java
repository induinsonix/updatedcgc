package com.thechandigarhgolfclub;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.thechandigarhgolfclub.Login.Login_screen;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONException;
import org.json.JSONObject;


public class CCBActivity extends Activity implements ConnectivityReceiveListener,Iconstant
{    private boolean doubleBackToExitPressedOnce;
RelativeLayout id_circle_menu_item_center;
	ImageView logout;
	private CircleMenuLayout mCircleMenuLayout;
ImageView tea_booking,members_login,notice_board;
	private String[] mItemTexts = new String[] { "Course ", "Club", "Gallery",
			"Score Card", "News/Events", "Golf Calender" };
	private int[] mItemImgs = new int[] { R.drawable.bg_white1,
			R.drawable.bg_white1, R.drawable.bg_white1,
			R.drawable.bg_white1, R.drawable.bg_white1,
			R.drawable.bg_white1 };
	private int[] mItemicon = new int[] { R.drawable.course,
			R.drawable.club, R.drawable.gallery,
			R.drawable.scorecard, R.drawable.news,
			R.drawable.calender_black };
	SharedPreferences sharedPreference;
	private SharedPreferences.Editor editor;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		//自已切换布局文件看效果
//		setContentView(R.layout.activity_main02);
		setContentView(R.layout.activity_home);
		if (!AppController.connection)
			AppController.getInstance(getApplicationContext()).checkConnection(this);
		AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
				.setLabel("Main Activity")
				.build());
		id_circle_menu_item_center=(RelativeLayout)findViewById(R.id.id_circle_menu_item_center);
		tea_booking=(ImageView)findViewById(R.id.tea_booking);
		notice_board=(ImageView)findViewById(R.id.notice_board);
		members_login=(ImageView)findViewById(R.id.members_login);
		logout=(ImageView)findViewById(R.id.logout);
		sharedPreference= PreferenceManager.getDefaultSharedPreferences(CCBActivity.this);
		mCircleMenuLayout = (CircleMenuLayout) findViewById(R.id.id_menulayout);
		mCircleMenuLayout.setMenuItemIconsAndTexts(mItemImgs, mItemTexts, mItemicon);

		sharedPreference= PreferenceManager.getDefaultSharedPreferences(CCBActivity.this);
		editor = sharedPreference.edit();

		Singleton.getinstance().logout_chek(sharedPreference,logout);


		//logout
		logout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (AppController.connection) {
					Singleton.getinstance().logout(CCBActivity.this, editor);
				} else {
					showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
				}
			}
		});

		tea_booking.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(CCBActivity.this, Teatime_booking.class);
				startActivity(intent);
				finish();
			}
		});

		notice_board.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(CCBActivity.this, Noticeboard.class);
				startActivity(intent);
//				finish();
			}
		});
		doubleBackToExitPressedOnce = false;

		members_login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (sharedPreference.getString("members_detail","").equals("1")){
					Intent intent = new Intent(CCBActivity.this, AccountDetails.class);

					startActivity(intent);
					finish();
				}
				else{
					Intent intent = new Intent(CCBActivity.this, Login_screen.class);

					startActivity(intent);
					finish();
				}


			}
		});


		mCircleMenuLayout.setOnMenuItemClickListener(new CircleMenuLayout.OnMenuItemClickListener()
		{
			
			@Override
			public void itemClick(View view, int pos)
			{

				if (pos==0){
					Intent intent=new Intent(CCBActivity.this,Course.class);
					overridePendingTransition(0,0);
					startActivity(intent);

					//finish();
				}
				else if (pos==1){
					Intent intent=new Intent(CCBActivity.this,Club.class);
					overridePendingTransition(0, 0);
					startActivity(intent);

					//finish();
				}
			else	if (pos==2){
					Intent intent=new Intent(CCBActivity.this,Gallery.class);
					overridePendingTransition(0, 0);
					startActivity(intent);

					//finish();
				}
				else	if (pos==3){
					Intent intent=new Intent(CCBActivity.this,About.class);
					overridePendingTransition(0, 0);
					startActivity(intent);
				//	finish();
				}
				else	if (pos==4){
					Intent intent=new Intent(CCBActivity.this,News_events.class);
					overridePendingTransition(0, 0);
					startActivity(intent);

				//	finish();
				}

else if (pos==5){
					Intent intent=new Intent(CCBActivity.this,Tournaments.class);
					overridePendingTransition(0, 0);
					startActivity(intent);
			}
			}

			@Override
			public void itemCenterClick(View view)
			{	json_home();
				Intent intent=new Intent(CCBActivity.this,Home.class);
				startActivity(intent);

				//finish();
			}
		});
		
	}

	@Override
	public void onBackPressed() {
		if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
			finish();
			return;
		}
		else {
			this.doubleBackToExitPressedOnce = true;
			Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
	}

	@Override
	public void onNetworkConnectionChanged(boolean isConnected) {
		getConnection(isConnected);
		AppController.connection = isConnected;
	}

	private void getConnection(Boolean isConnected) {
		if (isConnected) {
			showSnackAlert(logout, getString(R.string.dialog_message_internet));
		} else
			showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
	}

	private void showSnackAlert(@NonNull View view, @NonNull String message) {
		Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
		ViewGroup group = (ViewGroup) snack.getView();
		group.setBackgroundColor(Color.RED);
		TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
		tv.setTextColor(Color.WHITE);
		snack.show();
	}

	void json_home(){

		String url=Baseurl+"home.php?page=home";
		Log.d("abouturl", url);
		StringRequest stringRequest=new StringRequest(url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				String plainText = Html.fromHtml(response).toString();
				try {
					JSONObject jsonObject=new JSONObject(plainText);
					String status=jsonObject.getString("status");
					if (status.equalsIgnoreCase("true")){
//						Spanned sp = Html.fromHtml(jsonObject.getString("data"));
//						String html ="<html><body style=text-align:justify>"+Html.toHtml(sp)+"</body></html>";
//						text_home.setBackgroundColor(getResources().getColor(R.color.background_textview));
//
//						text_home.setWebViewClient(new WebViewClient(){
//							public boolean shouldOverrideUrlLoading(WebView view, String url) {
//								view.loadUrl(url);
//								return true;
//							}
//							public void onPageFinished(WebView view, String url) {
//								super.onPageFinished(view,url);
//								pdf_lay.setVisibility(View.VISIBLE);
//							}
//						});
//						text_home.getSettings().setJavaScriptEnabled(true);
//						text_home.loadDataWithBaseURL(null,html, "text/html", "UTF-8",null);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {

			}
		});
		stringRequest.setRetryPolicy(new DefaultRetryPolicy(
				20000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
	//	stringRequest.setShouldCache(true);
		//    AppController.getInstance(getApplicationContext()).getRequestQueue().getCache().invalidate(Baseurl+"home.php?page=home", true);
		AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);

	}


}
