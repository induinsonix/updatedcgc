package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.NotificationListPojo;
import com.thechandigarhgolfclub.model.NotificationPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Notification_list extends Activity implements Iconstant, ConnectivityReceiveListener {
    ListView noti_list;
    ArrayList<HashMap<String, String>> array_noti;
    HashMap<String, String> map;
    private SharedPreferences sharedPreferences1;
    private ProgressDialog progressDialog;
    private ImageView icon_app;
    private static final long RIPPLE_DURATION = 250;
    RelativeLayout toolbar;
    private FrameLayout root;
    View contentHamburger;
    private ImageView notification;
    private BadgeView badgeView;
    private TextView title;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    View guillotineMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Notifications List Activity")
                .build());
        root = (FrameLayout) findViewById(R.id.root);
        noti_list = (ListView) findViewById(R.id.noti_list);
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        logout = (ImageView) findViewById(R.id.logout);
        title.setText("Notifications");
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Notification_list.this);
        editor = sharedPreference.edit();
        Singleton.getinstance().logout_chek(sharedPreference, logout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Notification_list.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Notification_list.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Notification_list.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }

        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, Notification_list.class);
                startActivity(intent);
                finish();
            }
        });
        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "notificationlist_data"))) {
            noti_status(true);
        } else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "notificationlist_data");
            Log.e("plain", object);
            NotificationListPojo mResponseObject1 = new Gson().fromJson(object, NotificationListPojo.class);
            setData(mResponseObject1);
            noti_status(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }


        if (!AppController.connection) {
            showSnackAlert(noti_list, getString(R.string.dialog_message_no_internet));
        }

        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);

        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, About.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, Course.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, Tournaments.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, Club.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, Gallery.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_list.this, News_events.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
    }

    private void setData(NotificationListPojo mResponseObject1) {
        try {
            String status = mResponseObject1.getStatus();

            if (status.equals("true")) {
                if (mResponseObject1.getData().size() == 0) {
                    Toast.makeText(Notification_list.this, "Not recevied yet any notification", Toast.LENGTH_LONG).show();
                } else {
                    for (int i = 0; i < mResponseObject1.getData().size(); i++) {
                        map = new HashMap<>();

                        String message = mResponseObject1.getData().get(i).getMsg();
                        String id = mResponseObject1.getData().get(i).getId();
                        String title = mResponseObject1.getData().get(i).getSubject();
                        String filename = mResponseObject1.getData().get(i).getFilename();
                        String status1 = mResponseObject1.getData().get(i).getStatus();
                        String date_time = mResponseObject1.getData().get(i).getSentOn();
                        map.put("message", message);
                        map.put("id", id);
                        map.put("title", title);
                        map.put("image", filename);
                        map.put("status", status1);
                        map.put("date_time", date_time);
                        array_noti.add(map);
                    }
                    Noti_adapter noti_adapter = new Noti_adapter(Notification_list.this, array_noti);
                    noti_list.setAdapter(noti_adapter);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //json of notification_status

    void noti_status(boolean b) {
        if (b) {
            showProgressDialog(getString(R.string.notification_loading));
        }
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        array_noti = new ArrayList<>();
        String url = Baseurl + "getallnotification.php?phone_number=" + sharedPreferences1.getString("phn", "");
        Log.d("url", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("res", response.toString());
                Singleton.getinstance().saveValue(getApplicationContext(), "notificationlist_data", response.toString());
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(), "notificationlist_data"));
                NotificationListPojo mResponseObject = new Gson().fromJson(response.toString(), NotificationListPojo.class);
                setData(mResponseObject);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "notificationlist_data");
                Log.e("plain", object);
                NotificationListPojo mResponseObject1 = new Gson().fromJson(object, NotificationListPojo.class);
                setData(mResponseObject1);
                closeProgressDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            noti_status(false);
            Singleton.getinstance().noti_count(Notification_list.this, badgeView);
            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    //Notification adapter
    class Noti_adapter extends BaseAdapter {

        Context context;
        ArrayList<HashMap<String, String>> arrayList;

        public Noti_adapter(Context context, ArrayList<HashMap<String, String>> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.notification_row, null);
            TextView title = (TextView) convertView.findViewById(R.id.title);
            TextView message = (TextView) convertView.findViewById(R.id.message);
            TextView date_time = (TextView) convertView.findViewById(R.id.date_time);
            ImageView noti_lay = (ImageView) convertView.findViewById(R.id.notification_count);
            title.setText(arrayList.get(position).get("title"));
            message.setText(arrayList.get(position).get("message"));
            date_time.setText(arrayList.get(position).get("date_time"));
            if (arrayList.get(position).get("status").equals("1")) {
                noti_lay.setVisibility(View.GONE);
            } else {
                noti_lay.setVisibility(View.VISIBLE);
            }
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //      cancelNotification(Notification_list.this, Integer.parseInt(arrayList.get(position).get("id")));
                    Intent intent = new Intent(Notification_list.this, Notification_membal.class);
                   // if (arrayList.get(position).get("image")==null) {
                    if (arrayList.get(position).get("image")==null) {
                        intent.putExtra("img", "null");
                    } else {
                        intent.putExtra("img", arrayList.get(position).get("image"));
                    }
                    intent.putExtra("notification", "0");
                    intent.putExtra("title", arrayList.get(position).get("title"));
                    intent.putExtra("message", arrayList.get(position).get("message"));
                    intent.putExtra("id", arrayList.get(position).get("id"));
                    startActivity(intent);
                    finish();
                }
            });


            return convertView;
        }

    }
}
