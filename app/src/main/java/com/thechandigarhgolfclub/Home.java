package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter1;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.MySharedPreferences;
import com.thechandigarhgolfclub.Utils.Singleton;

import org.json.JSONException;
import org.json.JSONObject;

import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.CoursePojo;
import com.thechandigarhgolfclub.model.HomePojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import java.io.UnsupportedEncodingException;


/**
 * Created by insonix on 18/10/16.
 */
public class Home extends Activity implements Iconstant, ConnectivityReceiveListener {

    private static final long RIPPLE_DURATION = 250;

    RelativeLayout toolbar;

    FrameLayout root;
    TextView title, policy_onlinepayment, msg_president, managementcommte;
    WebView text_home;
    View contentHamburger;
    private ProgressDialog progressDialog;
    private MySharedPreferences mySharedPreference;
    private String token;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    private ImageView notification;
    LinearLayout pdf_lay;
    private BadgeView badgeView;
    private HomePojo mResponseObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Home Activity")
                .build());
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        text_home = (WebView) findViewById(R.id.text_home);
        msg_president = (TextView) findViewById(R.id.msg_president);
        managementcommte = (TextView) findViewById(R.id.managementcommte);
        pdf_lay = (LinearLayout) findViewById(R.id.pdf_lay);
        //     progressDialog=new ProgressDialog(Home.this);
        policy_onlinepayment = (TextView) findViewById(R.id.policy_onlinepayment);
        title.setText("Home");

        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Home.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        //logout
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Home.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
//        Singleton.getinstance().logout_chek(sharedPreference, logout);
//        Cache.Entry cache = AppController.getInstance(getApplicationContext()).getRequestQueue().getCache().get(Baseurl+"home.php?page=home");
//   //     AppController.getInstance().getRequestQueue().getCache().invalidate(Baseurl+"home.php?page=home", true);
//        if (cache != null) {
//
//            findViewById(R.id.rl_noData).setVisibility(View.GONE);
//            Log.e("cache","cache exist");
//            try {
//                String data = new String(cache.data, "UTF-8");
//                Log.d("Best_friendz.this", "Inside" + data.toString());
//                String plainText = Html.fromHtml(data).toString();
//                try {
//                    JSONObject jsonObject=new JSONObject(plainText);
//                    String status=jsonObject.getString("status");
//                    if (status.equalsIgnoreCase("true")){
//                        Spanned sp = Html.fromHtml(jsonObject.getString("data"));
//                        String html ="<html><body style=text-align:justify>"+Html.toHtml(sp)+"</body></html>";
//                        text_home.setBackgroundColor(getResources().getColor(R.color.background_textview));
//
//                        text_home.setWebViewClient(new WebViewClient(){
//                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                                view.loadUrl(url);
//                                return true;
//                            }
//                            public void onPageFinished(WebView view, String url) {
//                                super.onPageFinished(view,url);
//                                pdf_lay.setVisibility(View.VISIBLE);
//                            }
//                        });
//                        text_home.getSettings().setJavaScriptEnabled(true);
//                        text_home.loadDataWithBaseURL(null,html, "text/html", "UTF-8",null);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                //   setCategoryData(new JSONObject(data));
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        } else {
//            if (AppController.connection) {
//                json_home();
//            } else {
//                findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
//                showSnackAlert(text_home, getString(R.string.dialog_message_no_internet));
//            }
////// Api Call
//        }
        if(TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(),"home_data"))) {
            HomePojo mResponseObject1=json_home(true);
            if(mResponseObject1!=null)
            {
                setData(mResponseObject1);
            }
        }
        else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "home_data");
            Log.e("plain",object);
            mResponseObject = new Gson().fromJson(object, HomePojo.class);
            setData(mResponseObject);
             HomePojo mResponseObject1= json_home(false);
            if(mResponseObject1!=null)
            {
                setData(mResponseObject1);
            }
           // json_home(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }


        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Home.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Home.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }

        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Notification_list.class);
                startActivity(intent);
                //     finish();
            }
        });

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });


        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

        managementcommte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   getFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment1()).commit();



                Intent intent = new Intent(Home.this, HomeImageFragment.class);
             //   intent.putExtra("url", "http://www.chandigarhgolfclub.in/Uploads/Editor/letter_president.pdf");
                startActivity(intent);
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.chandigarhgolfclub.in/Uploads/Editor/letter_president.pdf"));
//                startActivity(browserIntent);
            }
        });


        policy_onlinepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.chandigarhgolfclub.in/Uploads/Editor/Terms_conditions_online_payment.pdf"));
                Intent intent = new Intent(Home.this, Pdfview.class);
                intent.putExtra("url", "http://www.chandigarhgolfclub.in/Uploads/Editor/Terms_conditions_online_payment.pdf");
                startActivity(intent);
            }
        });


    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }

//    public void volleyJsonObjectRequest(String url){
//
//        String  REQUEST_TAG = "com.androidtutorialpoint.volleyJsonObjectRequest";
//        progressDialog.setMessage("info...");
//        progressDialog.show();
//        StringRequest stringRequest=new StringRequest(url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                String plainText = Html.fromHtml(response).toString();
//                try {
//                    JSONObject jsonObject=new JSONObject(plainText);
//                    String status=jsonObject.getString("status");
//                    if (status.equalsIgnoreCase("true")){
//                        Spanned sp = Html.fromHtml(jsonObject.getString("data"));
//                        String html ="<html><body style=text-align:justify>"+Html.toHtml(sp)+"</body></html>";
//                        Log.d("cachechek",html);
////                        LayoutInflater li = LayoutInflater.from(Home.this);
////                        showDialogView = li.inflate(R.layout.show_dialog, null);
////                        outputTextView = (TextView)showDialogView.findViewById(R.id.text_view_dialog);
////                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
////                        alertDialogBuilder.setView(showDialogView);
////                        alertDialogBuilder
////                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
////                                    public void onClick(DialogInterface dialog, int id) {
////                                    }
////                                })
////                                .setCancelable(false)
////                                .create();
////                        outputTextView.setText(html);
////                        alertDialogBuilder.show();
//                        progressDialog.hide();
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//
//            }
//        });
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
//                20000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest,REQUEST_TAG);
////        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectReq,REQUEST_TAG);
//
//    }
//
//
//
//    //json of home

    HomePojo json_home(boolean b) {
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        if(b)
        {
            showProgressDialog("Loading..");
        }

        String url = Baseurl + "home.php?page=home";
        Log.d("abouturl", url);

//        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//           //     String plainText = Html.fromHtml(String.valueOf(response)).toString();
//                try {
//           //         JSONObject jsonObject=new JSONObject(plainText);
//                    String status=response.getString("status");
//                    if (status.equalsIgnoreCase("true")){
//                        Spanned sp = Html.fromHtml(response.getString("data"));
//                        String html ="<html><body style=text-align:justify>"+Html.toHtml(sp)+"</body></html>";
//                        text_home.setBackgroundColor(getResources().getColor(R.color.background_textview));
//
//                        text_home.setWebViewClient(new WebViewClient(){
//                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                                view.loadUrl(url);
//                                return true;
//                            }
//                            public void onPageFinished(WebView view, String url) {
//                                super.onPageFinished(view,url);
//                                pdf_lay.setVisibility(View.VISIBLE);
//                            }
//                        });
//                        text_home.getSettings().setJavaScriptEnabled(true);
//                        text_home.loadDataWithBaseURL(null,html, "text/html", "UTF-8",null);
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                progressDialog.dismiss();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//Log.d("error", String.valueOf(error));
//            }
//        });

        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text",plainText);
                Singleton.getinstance().saveValue(getApplicationContext(),"home_data", plainText);
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(),"home_data"));
               mResponseObject = new Gson().fromJson(plainText, HomePojo.class);
                setData(mResponseObject);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "home_data");
                Log.e("plain",object);
                 mResponseObject = new Gson().fromJson(object, HomePojo.class);
                setData(mResponseObject);
                closeProgressDialog();

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(true);
        AppController.getInstance(getApplicationContext()).getRequestQueue().getCache().invalidate(Baseurl + "home.php?page=home", true);
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
        return mResponseObject;
    }

    private void setData(HomePojo mResponseObject) {
        String status = mResponseObject.getStatus();
        if (status.equalsIgnoreCase("true")) {
            Spanned sp = Html.fromHtml(mResponseObject.getData());
            String html = "<html><body style=text-align:justify>" + Html.toHtml(sp) + "</body></html>";
            text_home.setBackgroundColor(getResources().getColor(R.color.background_textview));

            text_home.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    pdf_lay.setVisibility(View.VISIBLE);
                }
            });
            text_home.getSettings().setJavaScriptEnabled(true);
            text_home.loadDataWithBaseURL(null, html, "text/html", "UTF-8", null);
        }
        else
        {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "home_data");
            Log.e("plain",object);
            HomePojo mResponseObject1 = new Gson().fromJson(object, HomePojo.class);
            setData(mResponseObject1);
        }

    }


    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    public void volleyCacheRequest(String url) {
        Cache cache = AppController.getInstance(getApplicationContext()).getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                // handle data, like converting it to xml, json, bitmap etc.,
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {

        }
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            json_home(false);
            Singleton.getinstance().noti_count(Home.this, badgeView);

            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }
}
