package com.thechandigarhgolfclub.Utils;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.thechandigarhgolfclub.receiver.ConnectionReceiver;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import java.util.concurrent.TimeUnit;


public class AppController extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "IkKXE9kkmb9BW3hyCAU6u4Mt3";
    private static final String TWITTER_SECRET = "DZtQY7ddtugp2zeQpueUOSg60hCi7bvllkvqPnNe5vJOpUMfRz";

    private ConnectionReceiver receiver;
    private static GoogleAnalytics analytics;

    private static Tracker tracker;
    public static final String TAG = AppController.class
            .getSimpleName();
    public static Boolean connection = false;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;
    public static Tracker tracker() {
        return tracker;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        //TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
      //  Fabric.with(this, new TwitterCore(authConfig), new Digits.Builder().build(), new Crashlytics());
        MultiDex.install(this);
        mInstance = this;
        mRequestQueue=getRequestQueue();
        analytics = GoogleAnalytics.getInstance(this);
// TODO: Replace the tracker-id with your app one from https://www.google.com/analytics/web/
        tracker = analytics.newTracker("UA-87143942-1");
// Provide unhandled exceptions reports. Do that first after creating the tracker
        tracker.enableExceptionReporting(true);
// Enable Remarketing, Demographics & Interests reports
// https://developers.google.com/analytics/devguides/collection/android/display-features
        tracker.enableAdvertisingIdCollection(true);
// Enable automatic activity tracking for your app
        tracker.enableAutoActivityTracking(true);
        receiver = new ConnectionReceiver();
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public static synchronized AppController getInstance(Context applicationContext) {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // Don't forget to start the volley request queue
         //   mRequestQueue.start();
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public void setConnectivityListener(ConnectivityReceiveListener listener) {
        ConnectionReceiver.connectivityReceiverListener = listener;
    }

    public void checkConnection(@NonNull Context context) {
        receiver.checkInternetConnection(context);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
    public ImageLoader getImageLoader() {
        return mImageLoader;
    }
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}