package com.thechandigarhgolfclub.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.thechandigarhgolfclub.Notification_membal;
import com.thechandigarhgolfclub.R;
import com.thechandigarhgolfclub.SplashScreen;
import com.thechandigarhgolfclub.receiver.FcmBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

/**
 * Created by Akshay Raj on 5/31/2016.
 * Snow Corporation Inc.
 * www.snowcorp.org
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SharedPreferences sharedPreference, sharedPreferences1;
    NotificationCompat.Builder notificationBuilder;

    Bitmap image;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editor1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        sharedPreference = PreferenceManager.getDefaultSharedPreferences(MyFirebaseMessagingService.this);
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        editor1=sharedPreferences1.edit();
        editor=sharedPreference.edit();
Log.d("fcmdata", String.valueOf(remoteMessage.getData()));
        String tag = remoteMessage.getData().get("tag");
        String msg = remoteMessage.getData().get("message");
        String title = remoteMessage.getData().get("title");
        String img = remoteMessage.getData().get("icon");
        String memcode = remoteMessage.getData().get("memcode");
        String notify_id = remoteMessage.getData().get("notify_id");

//
        image = getBitmapFromURL(img);

        sendNotification(tag, msg, title, img, image, memcode, notify_id);
    }

    private void sendNotification(String tag, String messageBody, String title, String img1, Bitmap img, String memcode, String notify_id) {
        Random r = new Random();
        int id11 = r.nextInt(9999 - 0 + 1);
//Log.d("imageee",img1);
        if (!tag.equals("delete")) {
            editor = sharedPreference.edit();
            editor.putInt("notification", sharedPreference.getInt("notification", 0) + 1);
            editor.commit();
            Intent intent = new Intent(this, Notification_membal.class);
            intent.putExtra("notification", "1");
            if (tag.equals("image")) {
                intent.putExtra("memcode", "null");
                intent.putExtra("img", img1);
            } else {
                intent.putExtra("memcode", memcode);
                intent.putExtra("img", "null");
            }
            intent.putExtra("message", messageBody);
            intent.putExtra("notifyId", id11);
            intent.putExtra("title", title);
            intent.putExtra("id", notify_id);
            intent.putExtra("tag",tag);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            if (tag.equalsIgnoreCase("image") && !img1.equals("")) {
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.home)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(img))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            } else {
                notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.home)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
            }

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            notificationManager.notify(id11 /* ID of notification */, notificationBuilder.build());
            FcmBroadcastReceiver.completeWakefulIntent(intent);
        } else if (tag.equals("delete")){

            sharedPreference = PreferenceManager.getDefaultSharedPreferences(MyFirebaseMessagingService.this);
            sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);

            editor1.clear().commit();
            editor.clear().commit();

            Intent intent1 = new Intent(MyFirebaseMessagingService.this, SplashScreen.class);
                       intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);
            FcmBroadcastReceiver.completeWakefulIntent(intent1);
        }


    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }



}