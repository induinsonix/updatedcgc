package com.thechandigarhgolfclub.Utils;

/**
 * Created by insonix on 24/8/16.
 */
public class Config implements Iconstant {
    // server URL configuration
//    public static final String URL_REQUEST_SMS = "http://demo.insonix.com/bush/send_otp.php";
//    public static final String URL_VERIFY_OTP = " http://demo.insonix.com/bush/verify_otp.php";
   // public static final String URL_REQUEST_SMS = "http://demo.insonix.com:81/smart_crop/android_sms/request_sms.php";
    public static final String URL_VERIFY_OTP = Baseurl+"verify_otp.php";

    // SMS provider identification
    // It should match with your SMS gateway origin
    // You can use  MSGIND, TESTER and ALERTS as sender ID
    // If you want custom sender Id, approve MSG91 to get one
    public static final String SMS_ORIGIN = "5757537";

    // special character to prefix the otp. Make sure this character appears only once in the sms
    public static final String OTP_DELIMITER = ":";
}
