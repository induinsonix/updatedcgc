package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.DatePickerDialogFragment;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.model.AccountDetailsBean;
import com.thechandigarhgolfclub.model.LedgerDetailsPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class LedgerDetail extends Activity implements DatePickerDialog.OnDateSetListener, Iconstant, ConnectivityReceiveListener {

    private ListView ledger_list;
    Button date_submit;
    private SharedPreferences sharedPreference;
    private ArrayList<HashMap<String, String>> list_ledger;
    private ProgressDialog progressDialog;
    HashMap<String, String> map;
    private Calendar calendar;
    private EditText start_date;
    private EditText end_date;
    TextView title;
    ImageView back_btn, search_date;
    private Date date;
    LinearLayout search_lay;
    String chek_date;
    private int cur;
    private String current_date, end_date_value, start_date_value;
    private int year, month, day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger_detail);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        ledger_list = (ListView) findViewById(R.id.ledger_list);
        date_submit = (Button) findViewById(R.id.date_submit);
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(LedgerDetail.this);
        list_ledger = new ArrayList<>();
        start_date = (EditText) findViewById(R.id.start_date);
        end_date = (EditText) findViewById(R.id.end_date);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        search_date = (ImageView) findViewById(R.id.search_date);
        search_lay = (LinearLayout) findViewById(R.id.search_lay);
        title = (TextView) findViewById(R.id.title);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        chek_date = "0";
        String url = Baseurl_member + "MembersLedger?memcode=" + sharedPreference.getString("MemCode", "");
        if(TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(),"ledger_details_data"))) {
            json_ledger(url,true);
        }
        else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "ledger_details_data");
            Log.e("plain",object);
            LedgerDetailsPojo mResponseObject1 = new Gson().fromJson(object, LedgerDetailsPojo.class);
            setData(mResponseObject1);
            json_ledger(url,false);
            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }

        if (!AppController.connection) {
            showSnackAlert(ledger_list, getString(R.string.dialog_message_no_internet));
        }


        date_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
                Date sdate = null, edate = null;

                try {
                    sdate = formate.parse(start_date.getText().toString());
                    edate = formate.parse(end_date.getText().toString());

                } catch (ParseException e) {


                }
                if (edate.compareTo(sdate) > 0) {
                    search_lay.setVisibility(View.GONE);
                    String url = Baseurl_member + "MembersLedger?memcode=" + sharedPreference.getString("MemCode", "") + "&sdate=" + formate.format(sdate) + "&edate=" + formate.format(edate);
                    if (AppController.connection) {
                        json_ledger(url, true);
                    } else {
                        findViewById(R.id.rl_noData).setVisibility(View.VISIBLE);
                        showSnackAlert(ledger_list, getString(R.string.dialog_message_no_internet));
                    }
                } else {

                    new AlertDialog.Builder(LedgerDetail.this)
                            .setTitle("Error")
                            .setMessage("End Date is always greater than Start date")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })

                            .show();

                }

            }
        });

        search_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                search_lay.setVisibility(View.VISIBLE);
                ledger_list.setVisibility(View.GONE);
                chek_date = "1";

            }
        });


        showDate(year, month + 1, day, "start");
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cur = DATE_PICKER_FROM;
                String start_text = start_date.getText().toString().trim();
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    date = format1.parse(start_text);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                Log.e("dateee", year + "-" + month + "-" + day);

                Log.e("curfrom", String.valueOf(cur));
                // setStartDate();
                showDatePicker(year, month, day, DATE_PICKER_FROM);

            }
        });
        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String end_text = end_date.getText().toString().trim();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    date = format.parse(end_text);
                    System.out.println(date);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                calendar.setTime(date);
                cur = DATE_PICKER_TO;
                Log.e("curto", String.valueOf(cur));
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                showDatePicker(year, month, day, DATE_PICKER_TO);


                //setEndDate();
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chek_date.equals("1")) {
                    Intent intent = new Intent(LedgerDetail.this, LedgerDetail.class);
                    intent.putExtra("Name", getIntent().getStringExtra("Name"));
                    //       intent.putExtra("Name", getIntent().getStringExtra("Name"));
                    intent.putExtra("categoryname", getIntent().getStringExtra("categoryname"));
                    intent.putExtra("memcode", getIntent().getStringExtra("memcode"));
                    intent.putExtra("membal", getIntent().getStringExtra("membal"));
                    //   intent.putExtra("categoryname", CategoryName);
                    // intent.putExtra("memcode", MemCode);
                    // intent.putExtra("membal", memBalance);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(LedgerDetail.this, AccountDetails.class);
                    startActivity(intent);
                    finish();
                }

            }
        });

    }

    private void setData(LedgerDetailsPojo response) {
        try {
            Boolean status = response.getStatus();
            String ErrMsg = response.getErrMsg();
            if (status.equals(true)) {
                for (int i = 0; i < response.getLedDetails().size(); i++) {
                    map = new HashMap<>();
                    String BillNum1 = response.getLedDetails().get(i).getBillNum();
                    String BillDate1 = response.getLedDetails().get(i).getBillDate();
                    String Narration1 = response.getLedDetails().get(i).getNarration();
                    String CompanyName1 = response.getLedDetails().get(i).getCompanyName();
                   // Float Amount1 = response.getLedDetails().get(i).getAmount();
                    Float DrAmount1 = response.getLedDetails().get(i).getDrAmount();
                    Float CrAmount1 = response.getLedDetails().get(i).getCrAmount();
                    float Balance1 = response.getLedDetails().get(i).getBalance();
                    String CrDr1 = response.getLedDetails().get(i).getCrDr();
                    if (DrAmount1 == 0) {
                        map.put("CrDr", "Credit");
                        map.put("Amount", "" + CrAmount1);
                    } else {
                        map.put("Amount", "" + DrAmount1);
                        map.put("CrDr", "Debit");
                    }
                    map.put("BillDate", BillDate1);
                    map.put("BillNum", BillNum1);
                    map.put("Narration", Narration1);
                    map.put("CompanyName", CompanyName1);
                    map.put("DrAmount", "" + DrAmount1);
                    map.put("CrAmount", "" + CrAmount1);
                    map.put("Balance", "" + Balance1);
                    list_ledger.add(map);
                }

                ledger_list.setVisibility(View.VISIBLE);
                ledgeradapter led_adapter = new ledgeradapter(list_ledger);
                ledger_list.setAdapter(led_adapter);

            } else {
                Toast.makeText(LedgerDetail.this, "" + ErrMsg, Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error",e.getMessage());
        }
    }


    private void showDatePicker(int year, int month, int day, int date) {
        Bundle b = new Bundle();
        b.putInt(DatePickerDialogFragment.YEAR, year);
        b.putInt(DatePickerDialogFragment.MONTH, month);
        b.putInt(DatePickerDialogFragment.DAY, day);
        b.putInt(DatePickerDialogFragment.DATE, date);
        DialogFragment picker = new DatePickerDialogFragment();
        picker.setArguments(b);
        picker.show(getFragmentManager(), "fragment_date_picker");
    }

    private void showDate(int year, int i, int day, String start) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        current_date = formatter.format(calendar.getTime());
        start_date.setText(current_date);
        end_date.setText(start_date.getText().toString());
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.year = year;
        this.month = monthOfYear;
        this.day = dayOfMonth;


        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Log.e("call", "onDateset " + cur);
        if (cur == DATE_PICKER_FROM) {
            Log.e("from", year + "-" + monthOfYear + 1 + "-" + dayOfMonth);
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            start_date_value = formatter.format(calendar.getTime());
            Log.e("string", start_date_value);
            start_date.setText(start_date_value);
            //end_date.setText(s);
        } else {
            Log.e("to", year + "-" + monthOfYear + "-" + dayOfMonth);
            Format formatter = new SimpleDateFormat("yyyy-MM-dd");
            end_date_value = formatter.format(calendar.getTime());
            end_date.setText(end_date_value);
        }
    }


    // json of ledger


    void json_ledger(String url, boolean b) {
        if(b)
        {
            showProgressDialog(getString(R.string.ledger_loading));

        }
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        list_ledger = new ArrayList<>();
        Log.d("uro", url);
//        Asyntask_ledger asyntask_ledger=new Asyntask_ledger(url);
//        asyntask_ledger.execute();
//        progressDialog.setMessage("Loading Account Statement..");
//        progressDialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                Singleton.getinstance().saveValue(getApplicationContext(),"ledger_details_data",response.toString());
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(),"ledger_details_data"));
              LedgerDetailsPojo mResponseObject = new Gson().fromJson(response.toString(),LedgerDetailsPojo.class);
                setData(mResponseObject);
                closeProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    public void onBackPressed() {
        if (chek_date.equals("1")) {
            Intent intent = new Intent(LedgerDetail.this, LedgerDetail.class);
            intent.putExtra("Name", getIntent().getStringExtra("Name"));
            intent.putExtra("categoryname", getIntent().getStringExtra("categoryname"));
            intent.putExtra("memcode", getIntent().getStringExtra("memcode"));
            intent.putExtra("membal", getIntent().getStringExtra("membal"));
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(LedgerDetail.this, AccountDetails.class);
            startActivity(intent);
            finish();
        }

    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {

            json_ledger(Baseurl_member + "MembersLedger?memcode=" + sharedPreference.getString("MemCode", ""), false);
            ;


            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    class Asyntask_ledger extends AsyncTask<Void, Void, ArrayList<HashMap<String, String>>> {
        String url;
        ProgressDialog progressDialog;

        public Asyntask_ledger(String url) {
            Log.d("ur;;", url);
            this.url = url;
            progressDialog = new ProgressDialog(LedgerDetail.this);
        }

        @Override
        protected void onPreExecute() {
            Log.d("AsyncTask!", "Showing dialog now!"); //shown in logcat
            progressDialog.setMessage("Loading..");
            progressDialog.show();
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(Void... params) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("res", response.toString());
                    try {
                        Boolean status = response.getBoolean("Status");
                        String ErrMsg = response.getString("ErrMsg");
                        Log.d("statusleder", status.toString());
                        if (status.equals(true)) {

                            JSONArray jsonArray = response.getJSONArray("LedDetails");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                map = new HashMap<>();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                String BillNum1 = jsonObject.getString("BillNum");
                                String BillDate1 = jsonObject.getString("BillDate");
                                String Narration1 = jsonObject.getString("Narration");
                                String CompanyName1 = jsonObject.getString("CompanyName");
                                int Amount1 = jsonObject.getInt("Amount");
                                int DrAmount1 = jsonObject.getInt("DrAmount");
                                int CrAmount1 = jsonObject.getInt("CrAmount");
                                int Balance1 = jsonObject.getInt("Balance");
                                String CrDr1 = jsonObject.getString("CrDr");
                                map.put("BillDate", BillDate1);
                                map.put("BillNum", BillNum1);
                                map.put("Narration", Narration1);
                                map.put("CompanyName", CompanyName1);
                                map.put("Amount", "" + Amount1);
                                map.put("DrAmount", "" + DrAmount1);
                                map.put("CrAmount", "" + CrAmount1);
                                map.put("Balance", "" + Balance1);
                                map.put("CrDr", CrDr1);
                                list_ledger.add(map);
                            }

                        } else {
                            Toast.makeText(LedgerDetail.this, "" + ErrMsg, Toast.LENGTH_LONG).show();
                        }
                        //  progressDialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("errorss", error.toString());
                    // progressDialog.dismiss();
                    Toast.makeText(LedgerDetail.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

            return list_ledger;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> list_ledger) {
            //   super.onPostExecute(aVoid);
            Log.d("loistt", list_ledger.toString());
            progressDialog.dismiss();
            ledger_list.setVisibility(View.VISIBLE);
            ledgeradapter led_adapter = new ledgeradapter(list_ledger);
            ledger_list.setAdapter(led_adapter);

        }
    }


    class ledgeradapter extends BaseAdapter {
        TextView billnum, BillDate, Narration, CompanyName, Amount, CrAmount, DrAmount, Balance, CrDr, Amount_text;
        TextView billnum1, Narration1, CompanyName1, Amount1, Amount_text1, billnum_text1, narration_text1, CompanyName_text1;
        ArrayList<HashMap<String, String>> list_ledger;
        CardView cardView;

        public ledgeradapter(ArrayList<HashMap<String, String>> list_ledger) {

            this.list_ledger = list_ledger;


        }

        @Override
        public int getCount() {
            return list_ledger.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(LedgerDetail.this);
            convertView = inflater.inflate(R.layout.ledger_row, null);
            cardView = (CardView) convertView.findViewById(R.id.card_view1);
            BillDate = (TextView) convertView.findViewById(R.id.BillDate);
            Narration = (TextView) convertView.findViewById(R.id.Narration);
            Narration1 = (TextView) convertView.findViewById(R.id.Narration1);
            narration_text1 = (TextView) convertView.findViewById(R.id.narration_text1);
            billnum = (TextView) convertView.findViewById(R.id.billnum);
            billnum1 = (TextView) convertView.findViewById(R.id.billnum1);
            billnum_text1 = (TextView) convertView.findViewById(R.id.billnum_text1);
            CompanyName = (TextView) convertView.findViewById(R.id.CompanyName);
            CompanyName1 = (TextView) convertView.findViewById(R.id.CompanyName1);
            CompanyName_text1 = (TextView) convertView.findViewById(R.id.CompanyName_text1);
            Amount = (TextView) convertView.findViewById(R.id.Amount);
            Amount1 = (TextView) convertView.findViewById(R.id.Amount1);
            CrAmount = (TextView) convertView.findViewById(R.id.CrAmount);
            DrAmount = (TextView) convertView.findViewById(R.id.DrAmount);
            Balance = (TextView) convertView.findViewById(R.id.Balance);
            CrDr = (TextView) convertView.findViewById(R.id.CrDr);
            Amount_text = (TextView) convertView.findViewById(R.id.Amount_text);
            Amount_text1 = (TextView) convertView.findViewById(R.id.Amount_text1);
            if (position == 0) {

                if (!TextUtils.isEmpty(getIntent().getStringExtra("Name"))) {
                    Amount1.setText(getIntent().getStringExtra("Name"));
                    Amount_text1.setText("Name");
                }
                if (!TextUtils.isEmpty(getIntent().getStringExtra("categoryname"))) {
                    billnum1.setText(getIntent().getStringExtra("categoryname"));
                    billnum_text1.setText("Category");
                }

                if (!TextUtils.isEmpty(getIntent().getStringExtra("memcode"))) {
                    Narration1.setText(getIntent().getStringExtra("memcode"));
                    narration_text1.setText("Member Id");
                }
                if (!TextUtils.isEmpty(getIntent().getStringExtra("membal"))) {
                    CompanyName1.setText(getIntent().getStringExtra("membal"));
                    CompanyName_text1.setText("Balance");
                }


                if (!TextUtils.isEmpty(list_ledger.get(position).get("BillDate"))) {
                    BillDate.setText(list_ledger.get(position).get("BillDate"));
                }
                if (!TextUtils.isEmpty(list_ledger.get(position).get("BillNum"))) {
                    billnum.setText(list_ledger.get(position).get("BillNum"));
                }

                if (!TextUtils.isEmpty(list_ledger.get(position).get("Narration"))) {
                    Narration.setText(list_ledger.get(position).get("Narration"));
                }
                if (!TextUtils.isEmpty(list_ledger.get(position).get("CompanyName"))) {
                    CompanyName.setText(list_ledger.get(position).get("CompanyName"));
                }

                if (!TextUtils.isEmpty(list_ledger.get(position).get("CrDr"))) {
                    Amount_text.setText(list_ledger.get(position).get("CrDr"));
                }


                Amount.setText(list_ledger.get(position).get("Amount") + "");

            } else {
                cardView.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(list_ledger.get(position).get("BillDate"))) {
                    BillDate.setText(list_ledger.get(position).get("BillDate"));
                }
                if (!TextUtils.isEmpty(list_ledger.get(position).get("BillNum"))) {
                    billnum.setText(list_ledger.get(position).get("BillNum"));
                }

                if (!TextUtils.isEmpty(list_ledger.get(position).get("Narration"))) {
                    Narration.setText(list_ledger.get(position).get("Narration"));
                }
                if (!TextUtils.isEmpty(list_ledger.get(position).get("CompanyName"))) {
                    CompanyName.setText(list_ledger.get(position).get("CompanyName"));
                }

                if (!TextUtils.isEmpty(list_ledger.get(position).get("CrDr"))) {
                    Amount_text.setText(list_ledger.get(position).get("CrDr"));
                }


                Amount.setText(list_ledger.get(position).get("Amount") + "");
            }
            // CrAmount.setText(  list_ledger.get(position).get("CrAmount")+"");
            // DrAmount.setText( list_ledger.get(position).get("DrAmount")+"");
            // Balance.setText( list_ledger.get(position).get("Balance1")+"");
            return convertView;
        }
    }


}
