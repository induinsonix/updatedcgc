package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.ShowMsg;
import com.thechandigarhgolfclub.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.thechandigarhgolfclub.Utils.TouchImageView1;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.ClubPojo;
import com.thechandigarhgolfclub.model.NewsPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;


public class News_events extends Activity implements Iconstant, ConnectivityReceiveListener {
    RelativeLayout toolbar;
    FrameLayout root;
    private static final long RIPPLE_DURATION = 250;
    ListView news_events;
    View contentHamburger, view_news, view_events;
    private TextView title, news_tab, events_tab;
    private ProgressDialog progressDialog;
    ArrayList<HashMap<String, String>> news_list, events_list;
    HashMap map;
    NewsEvents_adapter newsevents_adapter;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    private ImageView notification;
    private BadgeView badgeView;
    String chek_news_events;
    String NewsEventsChek = "No AboutData Found";
    private ArrayList<Image> images;
    //ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_events);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("News/Events Activity")
                .build());
        chek_news_events = "News";
       // Singleton.getinstance().getValue(getApplicationContext(), "news_data").equals("");
       // Singleton.getinstance().getValue(getApplicationContext(), "event_data").equals("");
        images = new ArrayList<>();
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
     //   progressBar=(ProgressBar)findViewById(R.id.progressBar);
        contentHamburger = findViewById(R.id.content_hamburger);
        view_news = findViewById(R.id.view_news);
        view_events = findViewById(R.id.view_events);
        title = (TextView) findViewById(R.id.title);
        news_tab = (TextView) findViewById(R.id.news_tab);
        news_events = (ListView) findViewById(R.id.news_events);
        events_tab = (TextView) findViewById(R.id.events_tab);
     //   progressBar.setMax(100);
        news_list = new ArrayList<>();
        events_list = new ArrayList<>();
        //   progressDialog = new ProgressDialog(News_events.this);
        title.setText("News/Events");

        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(News_events.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(News_events.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        Singleton.getinstance().logout_chek(sharedPreference, logout);


        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(News_events.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(News_events.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, Notification_list.class);
                startActivity(intent);
                // finish();
            }
        });

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "news_data"))) {
            json_news("News", true);
        } else {

            String object = Singleton.getinstance().getValue(getApplicationContext(), "news_data");
            Log.e("plain", object);
            NewsPojo mResponseObject1 = new Gson().fromJson(object, NewsPojo.class);
            setData(mResponseObject1, "News");
           json_news("News", false);
            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }
        if (!AppController.connection) {
            showSnackAlert(news_events, getString(R.string.dialog_message_no_internet));
        }

        news_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                view_news.setBackgroundColor(getResources().getColor(R.color.btn_text));
                view_events.setBackgroundColor(getResources().getColor(R.color.appbar));
                if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "news_data"))) {
                    json_news("News", true);
                } else {
                    String object = Singleton.getinstance().getValue(getApplicationContext(), "news_data");
                    Log.e("plain", object);
                    NewsPojo mResponseObject1 = new Gson().fromJson(object, NewsPojo.class);
                    setData(mResponseObject1, "News");
                   // json_news("News", false);
                    //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
                }
                if (!AppController.connection) {
                    showSnackAlert(news_events, getString(R.string.dialog_message_no_internet));
                }
                chek_news_events = "News";
            }
        });

        events_tab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_news.setBackgroundColor(getResources().getColor(R.color.appbar));
                view_events.setBackgroundColor(getResources().getColor(R.color.btn_text));
                if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "event_data"))) {
                    json_news("Event", true);
                } else {
                    String object = Singleton.getinstance().getValue(getApplicationContext(), "event_data");
                    Log.e("plain", object);
                    NewsPojo mResponseObject1 = new Gson().fromJson(object, NewsPojo.class);
                    setData(mResponseObject1, "Event");
                  //  json_news("Event", false);

                    //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
                }
                if (!AppController.connection) {
                    showSnackAlert(news_events, getString(R.string.dialog_message_no_internet));
                }
                chek_news_events = "Event";
            }
        });


        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(News_events.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });


        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();


    }



    // Kindly Provide your MemberId and register 10 digit mobile number.Password will be sent to your register mobile number
    // Password will be sent to your register mobile number



    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(News_events.this, CCBActivity.class);
//        startActivity(intent);
        finish();
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            if (chek_news_events.equals("Event"))
                json_news("Event", false);
            else
                json_news("News", false);
            Singleton.getinstance().noti_count(News_events.this, badgeView);
            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }
    //json of news

    void json_news(final String type1, boolean b) {
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        news_events.setAdapter(null);

        if (b) {
            showProgressDialog(getString(R.string.news_loading));
        }
        final String url = Baseurl + "news.php";
        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res", response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text", plainText);
                if (type1.equals("Event")) {
                    Singleton.getinstance().saveValue(getApplicationContext(), "event_data", plainText);

                } else {
                    Singleton.getinstance().saveValue(getApplicationContext(), "news_data", plainText);

                }
                // Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(),"news_data"));
                NewsPojo mResponseObject = new Gson().fromJson(plainText, NewsPojo.class);
                setData(mResponseObject, type1);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
               /* String object = Singleton.getinstance().getValue(getApplicationContext(), "news_data");
                Log.e("plain",object);
                NewsPojo mResponseObject1 = new Gson().fromJson(object, NewsPojo.class);
                setData(mResponseObject1,"News");*/
                //Toast.makeText(News_events.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


    }

    private void setData(NewsPojo mResponseObject, String type1) {
        news_list = new ArrayList<>();
        events_list = new ArrayList<>();
        try {
            String status = mResponseObject.getStatus();
            if (status.equalsIgnoreCase("true")) {
                for (int i = 0; i < mResponseObject.getData().size(); i++) {
                    map = new HashMap();
                    String type = mResponseObject.getData().get(i).getType();
                    String name = mResponseObject.getData().get(i).getName();
                    String image = mResponseObject.getData().get(i).getImage();
                    Spanned description = Html.fromHtml(mResponseObject.getData().get(i).getDescription());
                    //  String description = "<html><body style=text-align:justify>" + Html.toHtml(description1) + "</body></html>";
                    if (type.equalsIgnoreCase("event")) {
                        map.put("name", name);
                        map.put("image", image);
                        map.put("type", type);
                        map.put("description", description);
                        events_list.add(map);

                    } else {
                        map.put("name", name);
                        map.put("image", image);
                        map.put("type", type);
                        map.put("description", description);
                        news_list.add(map);
                    }
                }
                if (type1.equalsIgnoreCase("event") && !events_list.isEmpty()) {

                    newsevents_adapter = new NewsEvents_adapter(News_events.this, events_list);
                    news_events.setAdapter(newsevents_adapter);
                    NewsEventsChek = "News Not Found";
                } else if (type1.equalsIgnoreCase("news") && !news_list.isEmpty()) {
                    newsevents_adapter = new NewsEvents_adapter(News_events.this, news_list);
                    news_events.setAdapter(newsevents_adapter);
                    NewsEventsChek = "Events Not Found";
                } else {
                    // newsevents_adapter=new NewsEvents_adapter(News_events.this,news_list);
                    news_events.setAdapter(null);
                    new ShowMsg().createDialog(News_events.this, NewsEventsChek);
                }
            } else {
                new ShowMsg().createDialog(News_events.this, mResponseObject.getMessage());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class NewsEvents_adapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> news_eventslist;
        private ImageLoader imageLoader;
        private DisplayImageOptions options;


        NewsEvents_adapter(Context context, ArrayList<HashMap<String, String>> news_eventslist) {
            this.context = context;
            this.news_eventslist = news_eventslist;
        }

        @Override
        public int getCount() {
            return news_eventslist.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.news_events_row, null);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView description = (TextView) convertView.findViewById(R.id.description);
            LinearLayout layImage = (LinearLayout) convertView.findViewById(R.id.layImage);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.image_champ);
            name.setText(news_eventslist.get(position).get("name"));
            description.setText(news_eventslist.get(position).get("description"));
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(context));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
            String imageList[] = news_eventslist.get(position).get("image").split(",");
            imageLoader.displayImage(imageList[0], imageView, options);

            convertView.setOnClickListener(new View.OnClickListener() {
                public MyViewPagerAdapter myViewPagerAdapter;

                @Override
                public void onClick(View v) {
                    String imageList[] = news_eventslist.get(position).get("image").split(",");
                    images = new ArrayList<>();
                    for (int i = 0; i < imageList.length; i++) {
                        Image image = new Image();
                        image.setMedium(imageList[i].toString().trim());
                        images.add(image);
                    }
                    final Dialog settingsDialog = new Dialog(News_events.this, android.R.style.Theme_Holo_Dialog_NoActionBar);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.newseventspopup
                            , null));
                    int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                    int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.80);
                    settingsDialog.getWindow().setLayout(width, height);
                    settingsDialog.show();
                    TextView name = (TextView) settingsDialog.findViewById(R.id.name);
                    TextView description = (TextView) settingsDialog.findViewById(R.id.description);
                    //  ImageView imageView = (ImageView) settingsDialog.findViewById(R.id.image_champ);
                    ViewPager viewpager = (ViewPager) settingsDialog.findViewById(R.id.viewpager);
                    myViewPagerAdapter = new MyViewPagerAdapter();
                    viewpager.setAdapter(myViewPagerAdapter);
                    name.setText(news_eventslist.get(position).get("name"));
                    description.setText(news_eventslist.get(position).get("description"));
                }
            });
            return convertView;
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.image_fullscreen_preview, null);

            final TouchImageView1 imageViewPreview = (TouchImageView1
                    ) view.findViewById(R.id.image_preview);
//            Picasso
//                    .with(getActivity())
//                    .load(R.drawable.gallery)
//                    .resize(600, 200) // resizes the image to these dimensions (in pixel). does not respect aspect ratio
//                    .into(imageViewPreview);
            Image image = images.get(position);

            Glide.with(News_events.this).load(image.getMedium())
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.image_nodata)
                    .into(imageViewPreview);

            container.addView(view);
//            ScaleAnimation anim = new ScaleAnimation(1f, 1f, 1f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//            anim.setDuration(500);
//            anim.setFillAfter(true);
//            imageViewPreview.startAnimation(anim);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            return view;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
