package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.Singleton;

import java.util.ArrayList;


import com.thechandigarhgolfclub.Utils.SlideshowDialogFragment;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.ClubPojo;
import com.thechandigarhgolfclub.model.GalleryPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;


/**
 * Created by insonix on 17/10/16.
 */
public class  Gallery extends Activity implements Iconstant, ConnectivityReceiveListener {

    // GridView gridView;
    private static final long RIPPLE_DURATION = 250;

    RelativeLayout toolbar;
    int expanded_chek = 0;
    FrameLayout root;

    View contentHamburger;
    private TextView title;
    ImageView expandedImageView;
    private ProgressDialog progressDialog;
    private ArrayList<String> image_list;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    private ImageView notification;
    private BadgeView badgeView;
    private RecyclerView recyclerView;
    private ArrayList<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery1);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Gallery Activity")
                .build());
      //  Singleton.getinstance().saveValue(getApplicationContext(), "gallery_data","");
        expandedImageView = (ImageView) findViewById(R.id.expanded_image);
        // gridView = (GridView) findViewById(R.id.gridview);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        images = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        image_list = new ArrayList<>();
        title.setText("Gallery");

        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Gallery.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Gallery.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });


        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Gallery.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Gallery.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, Notification_list.class);
                startActivity(intent);
                //  finish();
            }
        });
        if(TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(),"gallery_data"))) {
            json_gallery(true);
        }
        else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "gallery_data");
            Log.e("plain",object);
            GalleryPojo mResponseObject1 = new Gson().fromJson(object, GalleryPojo.class);
            setData(mResponseObject1);
            json_gallery(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }


        if (!AppController.connection) {
            showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
        }

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, About.class);
                startActivity(intent);
                finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, Course.class);
                startActivity(intent);
                finish();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, Club.class);
                startActivity(intent);
                finish();
            }
        });
        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });
        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            json_gallery(true);
            Singleton.getinstance().noti_count(Gallery.this, badgeView);

            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    // json of gallery
    void json_gallery(boolean b) {
        if(b){
            showProgressDialog(getString(R.string.gallery_loading));}
            findViewById(R.id.rl_noData).setVisibility(View.GONE);
        String url = Baseurl + "web_gallery.php";
        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res", response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text", plainText);
                Singleton.getinstance().saveValue(getApplicationContext(),"gallery_data","");
                Singleton.getinstance().saveValue(getApplicationContext(), "gallery_data", plainText);
                GalleryPojo mResponseObject1 = new Gson().fromJson(plainText, GalleryPojo.class);
                setData(mResponseObject1);
                closeProgressDialog();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "gallery_data");
                Log.e("plain",object);
                GalleryPojo mResponseObject1 = new Gson().fromJson(object, GalleryPojo.class);
                setData(mResponseObject1);
                json_gallery(false);
                closeProgressDialog();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void  setData(GalleryPojo mResponseObject) {
        images.clear();
        try {
            Log.e("mresponse", mResponseObject.toString());
            String status = mResponseObject.getStatus();
            if (status.equalsIgnoreCase("true")) {
                for (int i = 0; i < mResponseObject.getImage().size(); i++) {

                    Image image = new Image();
                    image.setMedium(mResponseObject.getImage().get(i).toString());
                    images.add(image);
                }
                // mAdapter = new GalleryAdapter(getApplicationContext(), images);
                recyclerView.setAdapter(new GalleryAdapter(getApplicationContext(), images));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (expanded_chek == 1) {

            // expandedImageView.setVisibility(View.GONE);
            expanded_chek = 0;
            Intent intent = new Intent(Gallery.this, Gallery.class);
            startActivity(intent);
            finish();
        } else {
//            Intent intent = new Intent(Gallery.this, CCBActivity.class);
//            startActivity(intent);
            finish();
        }

    }
}


  /*  class ImageAdapter extends BaseAdapter {
        ImageLoader imageLoader;
        private Animator mCurrentAnimator;
        DisplayImageOptions options;
        private int mShortAnimationDuration = 700;
        boolean zoomedOut;
        private Context mContext;
        LayoutInflater inflater;
        ArrayList<String> image_list;
        List<ImageView> images;

        public ImageAdapter(Context c, ArrayList<String> image_list) {
            mContext = c;
            this.image_list = image_list;
            images = new ArrayList<ImageView>();
        }


        public int getCount() {
            return image_list.size();
        }

        public Object getItem(int position) {
            return image_list.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.row_gallery, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
            //      final ViewPager viewpager = (ViewPager) convertView.findViewById(R.id.pager);
            final ProgressBar pbHeaderProgress = (ProgressBar) convertView.findViewById(R.id.pbHeaderProgress);

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
            imageLoader.displayImage(image_list.get(position), imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(FailReason failReason) {
                    pbHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(Bitmap bitmap) {
                    pbHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled() {
                    pbHeaderProgress.setVisibility(View.GONE);
                }
            });

            //imageView.setBackground(getDrawable(mThumbIds[position]));
            imageView.setTag(image_list.get(position));


            zoomedOut = false;
            images.add(imageView);

            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    final Dialog settingsDialog = new Dialog(Gallery.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.gg
                            , null));
                    ImageView imageView1 = (ImageView) settingsDialog.findViewById(R.id.img);
                    ImageView back_btn = (ImageView) settingsDialog.findViewById(R.id.back_btn);
                    ScaleAnimation anim = new ScaleAnimation(1f, 1f, 1f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    anim.setDuration(500);
                    anim.setFillAfter(true);
                    imageView1.startAnimation(anim);
                    imageLoader.displayImage(image_list.get(position), imageView1, options);
                    settingsDialog.show();

                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            settingsDialog.dismiss();
                        }
                    });
                }
            });

            return convertView;
        }

        // References to our images in res > drawable


    }*/


