package com.thechandigarhgolfclub.receiver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class FcmBroadcastReceiver extends WakefulBroadcastReceiver implements Iconstant {


    @Override
    public void onReceive(Context context, Intent intent1) {
        noti_count(context);
    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    // json of noti count

    public void noti_count(final Context c1) {

        final SharedPreferences sharedPreferences1 = c1.getSharedPreferences("notdel", Context.MODE_PRIVATE);
//        showProgressDialog("Logging..",c1);
        String url = Baseurl + "badge_count.php?phone_number=" + sharedPreferences1.getString("phn", "");
        Log.d("url_phn", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    String status = response.getString("status");

                    if (status.equals("true")) {
                        int count = response.getInt("count");
                        Log.d("count", count + "");
                        if (count == 0) {
                        } else {

                            setBadge(c1, count);
                        }
                    }
                } catch (JSONException e) {
                    Log.d("eee", e + "");
                    e.printStackTrace();
                }
                //    closeProgressDialog();

                //progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();
                //  closeProgressDialog();


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(c1).addToRequestQueue(jsonObjectRequest);
    }

}
