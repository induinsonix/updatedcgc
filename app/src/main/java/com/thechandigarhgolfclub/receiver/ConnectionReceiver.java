package com.thechandigarhgolfclub.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;


import com.thechandigarhgolfclub.Utils.AppController;


public class ConnectionReceiver extends BroadcastReceiver {
    public static ConnectivityReceiveListener connectivityReceiverListener;
Context context;
    public ConnectionReceiver() {
        super();
    }

    @Override
    public void onReceive(@NonNull Context context, Intent intent) {
        checkInternetConnection(context);
        this.context=context;
    }

    private boolean isInternetConnected(@NonNull Context context) {
        this.context=context;
        if (isAirplaneModeOn()) {
            return false;
        } else {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            return info != null && info.isConnected();
        }
    }

    public void checkInternetConnection(@NonNull Context context) {
        this.context=context;
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener.onNetworkConnectionChanged(isNetworkAvailable(context));
        }
    }

    private boolean isNetworkAvailable(@NonNull Context context) {
        this.context=context;
        return isInternetConnected(context);
//        return isInternetConnected(context) && isInternetWorking();
    }

   /* private boolean isInternetWorking() {
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }*/

    @SuppressWarnings("deprecation")
    private boolean isAirplaneModeOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(AppController.getInstance(context).getContentResolver
                    (), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.System.getInt(AppController.getInstance(context).getContentResolver
                    (), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

}
