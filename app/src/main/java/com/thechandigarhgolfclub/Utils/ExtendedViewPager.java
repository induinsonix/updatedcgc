package com.thechandigarhgolfclub.Utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

/**
 * Created by insonix on 2/1/17.
 */

public class ExtendedViewPager extends ViewPager {

    public ExtendedViewPager(Context context) {
        super(context);
    }

    public ExtendedViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        Log.d("pos111", "position: " );
        if (v instanceof TouchImageView1) {
            Log.d("pos11100", "position: "+v );
            return ((TouchImageView1) v).canScrollHorizontallyFroyo(-dx);
        } else {
            Log.d("pos11122", "position: " );
            return super.canScroll(v, checkV, dx, x, y);
        }
    }

}