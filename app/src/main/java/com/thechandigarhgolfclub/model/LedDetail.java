
package com.thechandigarhgolfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LedDetail {

    private String BillNum;
    private String BillDate;
    private String Narration;
    private String CompanyName;
    private Float Amount;
    private Float CrAmount;
    private Float DrAmount;
    private Float Balance;
    private String CrDr;

    /**
     *
     * @return
     * The billNum
     */
    public String getBillNum() {
        return BillNum;
    }

    /**
     *
     * @param billNum
     * The BillNum
     */
    public void setBillNum(String billNum) {
        this.BillNum = billNum;
    }

    /**
     *
     * @return
     * The billDate
     */
    public String getBillDate() {
        return BillDate;
    }

    /**
     *
     * @param billDate
     * The BillDate
     */
    public void setBillDate(String billDate) {
        this.BillDate = billDate;
    }

    /**
     *
     * @return
     * The narration
     */
    public String getNarration() {
        return Narration;
    }

    /**
     *
     * @param narration
     * The Narration
     */
    public void setNarration(String narration) {
        this.Narration = narration;
    }

    /**
     *
     * @return
     * The companyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     *
     * @param companyName
     * The CompanyName
     */
    public void setCompanyName(String companyName) {
        this.CompanyName = companyName;
    }

    /**
     *
     * @return
     * The amount
     */
    public Float getAmount() {
        return Amount;
    }

    /**
     *
     * @param amount
     * The Amount
     */
    public void setAmount(Float amount) {
        this.Amount = amount;
    }

    /**
     *
     * @return
     * The crAmount
     */
    public Float getCrAmount() {
        return CrAmount;
    }

    /**
     *
     * @param crAmount
     * The CrAmount
     */
    public void setCrAmount(Float crAmount) {
        this.CrAmount = crAmount;
    }

    /**
     *
     * @return
     * The drAmount
     */
    public Float getDrAmount() {
        return DrAmount;
    }

    /**
     *
     * @param drAmount
     * The DrAmount
     */
    public void setDrAmount(Float drAmount) {
        this.DrAmount = drAmount;
    }

    /**
     *
     * @return
     * The balance
     */
    public Float getBalance() {
        return Balance;
    }

    /**
     *
     * @param balance
     * The Balance
     */
    public void setBalance(Float balance) {
        this.Balance = balance;
    }

    /**
     *
     * @return
     * The crDr
     */
    public String getCrDr() {
        return CrDr;
    }

    /**
     *
     * @param crDr
     * The CrDr
     */
    public void setCrDr(String crDr) {
        this.CrDr = crDr;
    }


}
