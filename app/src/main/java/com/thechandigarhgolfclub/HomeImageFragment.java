package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.Utils.TouchImageView1;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;

/**
 * Created by root on 3/5/17.
 */

public class HomeImageFragment extends Activity {
    TouchImageView1 touchImageView1;
    ImageView imageView,logout;
    private View mView;
    View contentHamburger;
    private static final long RIPPLE_DURATION = 250;


    RelativeLayout toolbar;

    FrameLayout root;
    TextView title, policy_onlinepayment, msg_president, managementcommte;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        mView = inflater.inflate(R.layout.fragment_home_image, container, false);
//        touchImageView1 = (TouchImageView1)mView.findViewById(R.id.touchImage);
//        imageView = (ImageView)mView.findViewById(R.id.imageView);
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                touchImageView1.setVisibility(View.VISIBLE);
//                imageView.setVisibility(View.GONE);
//            }
//        });
//        return mView;
//    }

    //
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_image);
        touchImageView1 = (TouchImageView1) findViewById(R.id.touchImage);
        imageView = (ImageView) findViewById(R.id.imageView);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                touchImageView1.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
            }
        });
        title.setText("Home");

        sharedPreference = PreferenceManager.getDefaultSharedPreferences(HomeImageFragment.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        //logout
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(HomeImageFragment.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });


        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeImageFragment.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
