package com.thechandigarhgolfclub;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thechandigarhgolfclub.Login.Login_screen;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.MySharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreen extends AppCompatActivity implements Iconstant {
    MySharedPreferences mySharedPreferences;
    SharedPreferences sharedPreference, sharedPreferences1;

   // private DigitsAuthButton phoneButton;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private String android_id;
    private SharedPreferences sharedPreferences11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
//        String phoneNumber = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getLine1Number();
//        Log.d("phoneNumber", "onCreate: "+phoneNumber);
        AppController.getInstance(getApplicationContext()).checkConnection(this);
        mySharedPreferences = new MySharedPreferences(getApplicationContext());
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        editor = sharedPreferences1.edit();
        progressDialog = new ProgressDialog(SplashScreen.this);
        android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("android", android_id);
        editor.putString("android_id", android_id);
        editor.commit();

//        sharedPreference = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);
//        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
//        sharedPreferences1.edit().clear();
//        sharedPreference.edit().clear();
//        sharedPreference.edit().commit();
//        sharedPreferences1.edit().commit();
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);

        if (sharedPreferences1.getString("phn_reg", "").equals("")) {
            Log.d("hello", "hii");
            if (weHavePermissionToReadMessages()) {
                setUpDigitsButton();
            } else {
                requestReadMessagesPermissionFirst();
            }

        } else if (sharedPreference.getString("login", "").equals("")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreen.this, Login_screen.class);
                    intent.putExtra("notification_chek", "0");
                    startActivity(intent);
                    finish();
                }
            }, 2000);

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashScreen.this, CCBActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 2000);


        }


    }










//Device id

    private boolean weHavePermissionToReadMessages() {
        return ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;

    }

    private void requestReadMessagesPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECEIVE_SMS)) {
            Toast.makeText(this, "We need permission so you can access your SMS.", Toast.LENGTH_LONG).show();
            requestForResultMessagesPermission();
        } else {
            requestForResultMessagesPermission();
        }
    }

    private void requestForResultMessagesPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.RECEIVE_SMS}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //  super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            setUpDigitsButton();

        } else {
            Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            setUpDigitsButton();

        }
    }

    private void setUpDigitsButton() {
        setContentView(R.layout.activity_splash_screen);

        Intent intent=new Intent(this,ActivityPhoneAuth.class);
        startActivity(intent);
        finish();
        /*//  if (weHavePermissionToReadMessages()) {
        phoneButton = (DigitsAuthButton) findViewById(R.id.phone_button);
        //    phoneButton.setC
        Digits.clearActiveSession();
        phoneButton.setAuthTheme(R.style.CustomDigitsTheme);




        AuthCallback callback = new AuthCallback() {
            @Override
            public void success(DigitsSession session, String phoneNumber) {
                if (Singleton.getinstance().isNetworkAvailable(SplashScreen.this)) {
                    jsonotp(android_id, phoneNumber);
                } else {
                    Singleton.getinstance().dialog_internetconnection(SplashScreen.this);
                }

            }

            @Override
            public void failure(DigitsException error) {
                Log.d("Digits", "Sign in with Digits failure", error);
            }
        };


        if (phoneButton != null) {
            phoneButton.setCallback(callback);
        }
        phoneButton.performClick();


*/





        // code to call signin screen directly
//        if (phoneButton != null) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            }, 2000);
//        }
//        phoneButton.setCallback(new AuthCallback() {
//            @Override
//            public void success(DigitsSession digitsSession, String phoneNumber) {
//                if (Singleton.getinstance().isNetworkAvailable(SplashScreen.this)) {
//                    jsonotp(android_id, phoneNumber);
//                } else {
//                    Singleton.getinstance().dialog_internetconnection(SplashScreen.this);
//                }
//
//
//
//
//            }
//
//            @Override
//            public void failure(DigitsException e) {
//
//            }
//        });
        //   phoneButton.performClick();
//        } else {
//
//            requestReadMessagesPermissionFirst();
//        }
    }

    void jsonotp(String android_id, final String phoneNumber) {
        progressDialog.setMessage("Creating Profile...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPreferences11 = getSharedPreferences("neverdel", Context.MODE_PRIVATE);
        //  Log.d("urll", sharedPreferences.getString("token", ""));
        String url = Baseurl + "register.php?phone_number=" + phoneNumber + "&device_id=" + sharedPreferences11.getString("tokenn", "") + "&android_id=" + android_id + "&member_code=&member_status=0&category=&email=&name=";
        Log.d("urll", url);
        //Log.d("sharedPre", sharedPreferences.getString("tokennn", ""));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response2", response.toString());
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d("status", status);
                    if (status.equals("true")) {
                        editor.putString("phn_reg", "1");
                        editor.putString("phn", phoneNumber.toString());
                        editor.commit();
                        Intent intent = new Intent(getApplicationContext(), Login_screen.class);
                        startActivity(intent);
                        finish();
                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(SplashScreen.this, "" + message, Toast.LENGTH_LONG).show();

                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                progressDialog.dismiss();
                Toast.makeText(SplashScreen.this, "Unable to create Profile.Please Try again.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
//                9000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

   /* @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }*/
}
