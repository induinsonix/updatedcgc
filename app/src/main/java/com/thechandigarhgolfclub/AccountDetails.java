package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.AccountDetailsBean;
import com.thechandigarhgolfclub.model.MemberBalancePojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;


public class AccountDetails extends Activity implements Iconstant, ConnectivityReceiveListener {
    private static final long RIPPLE_DURATION = 250;
    TextView name, balance, category, memcode, title;
    TextView account_detail, account_statement, pay_now;
    SharedPreferences sharedPreference;
    SharedPreferences.Editor editor;
    FrameLayout root;
    ImageView user_profile_photo;
    View guillotineMenu;
    String FatherName, CityName, BirthDate, EmailID, MobileNo, Name, address, CategoryName, memBalance, MemCode;
    String payNowBalance="";
    private ProgressDialog progressDialog;
    private RelativeLayout toolbar;
    private ImageView icon_app, logout;
    private View contentHamburger;
    private ImageView notification;
    private BadgeView badgeView;
    private SharedPreferences sharedPreferences1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_member);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Details Activity")
                .build());
        category = (TextView) findViewById(R.id.category);
        user_profile_photo = (ImageView) findViewById(R.id.user_profile_photo);
        balance = (TextView) findViewById(R.id.balance);
        name = (TextView) findViewById(R.id.name);
        memcode = (TextView) findViewById(R.id.memcode);
        pay_now = (TextView) findViewById(R.id.pay_now);
        logout = (ImageView) findViewById(R.id.logout);
        account_statement = (TextView) findViewById(R.id.account_statement);
        account_detail = (TextView) findViewById(R.id.account_detail);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(AccountDetails.this);
        editor = sharedPreference.edit();
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        title = (TextView) findViewById(R.id.title);
        title.setText("Details");
        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "account_details_data"))) {
            json_memdetail(true);
        } else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "account_details_data");
            Log.e("plain", object);
            AccountDetailsBean mResponseObject1 = new Gson().fromJson(object, AccountDetailsBean.class);
            setData(mResponseObject1);
            String object1 = Singleton.getinstance().getValue(getApplicationContext(), "account_balance_data");
            Log.e("plain", object1);
            MemberBalancePojo mResponseObject = new Gson().fromJson(object1, MemberBalancePojo.class);
            setBalanceData(mResponseObject);
            json_memdetail(false);
            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }

        if (!AppController.connection) {
            showSnackAlert(category, getString(R.string.dialog_message_no_internet));
        }

        account_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Members_detail.class);
                intent.putExtra("Name", Name);
                intent.putExtra("address", address);
                intent.putExtra("CityName", CityName);
                intent.putExtra("FatherName", FatherName);
                intent.putExtra("MobileNo", MobileNo);
                intent.putExtra("EmailID", EmailID);
                intent.putExtra("BirthDate", BirthDate);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });

        account_statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, LedgerDetail.class);
                intent.putExtra("Name", Name);
                intent.putExtra("categoryname", CategoryName);
                intent.putExtra("memcode", MemCode);
                intent.putExtra("membal", memBalance);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });


        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, PayNow.class);
                intent.putExtra(Balance,payNowBalance);
                intent.putExtra(MEMID,MemCode);
                intent.putExtra(CATEGORY,CategoryName);
                intent.putExtra(NAME,Name);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });

        //naviggation
        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, About.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                // finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Course.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //  finish();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Tournaments.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //finish();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Club.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                // finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Gallery.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, News_events.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                // finish();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();


        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, CCBActivity.class);
                startActivity(intent);
                finish();

            }
        });


        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(AccountDetails.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        Singleton.getinstance().logout_chek(sharedPreference, logout);

        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(AccountDetails.this, notification);
//
        if (AppController.connection) {

            Singleton.getinstance().noti_count(AccountDetails.this, badgeView);

        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountDetails.this, Notification_list.class);
                startActivity(intent);
                //   finish();
            }
        });
    }

    private void setData(AccountDetailsBean response) {
        DisplayImageOptions options;
        ImageLoader imageLoader;
        try {
            Boolean status = response.getStatus();
            Log.d("status", status.toString());
            if (status.equals(true)) {
                MemCode = response.getMemCode();
                Log.d("MemCode", MemCode.toString());

                BirthDate = response.getBirthDate();
                String MemberFirstName = response.getMemberFirstName();
                String MemberMiddleName = response.getMemberMiddleName();
                String MemberLastName = response.getMemberLastName();
                FatherName = response.getFatherName();
                CategoryName = response.getCategoryName();
                String CorporateName = response.getCorporateName();
                String Address1 = response.getAddress1();
                String Address2 = response.getAddress2();
                String Address3 = response.getAddress3();
                CityName = response.getCityName();
                String StateName = response.getStateName();
                String Phone1 = response.getPhone1();
                String Phone2 = response.getPhone2();
                String ImagePath = response.getImagePath();
                String MemberName = MemberFirstName + " " + MemberMiddleName + " " + MemberLastName;
                MobileNo = response.getMobileNo();
                EmailID = response.getEmailID();
                if (TextUtils.isEmpty(Address1) && (!TextUtils.isEmpty(Address2)) && (!TextUtils.isEmpty(Address3))) {
                    address = Address2 + "," + Address3;
                } else if (TextUtils.isEmpty(Address2)
                        && (!TextUtils.isEmpty(Address1)) && (!TextUtils.isEmpty(Address3))) {
                    address = Address1 + "," + Address3;
                } else if (TextUtils.isEmpty(Address3) && (!TextUtils.isEmpty(Address1)) && (!TextUtils.isEmpty(Address2))) {
                    address = Address1 + "," + Address2;
                } else {

                    address = Address1 + "," + Address2 + "," + Address3;
                }
                Name = MemberFirstName + " " + MemberMiddleName + " " + MemberLastName;
                String CreditLimit = response.getCreditLimit();
                category.setText("Category: " + CategoryName);
                memcode.setText("Member ID: " + MemCode);
                name.setText(MemberName);
                imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(AccountDetails.this));
                options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
                imageLoader.displayImage(ImagePath, user_profile_photo, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted() {

                    }

                    @Override
                    public void onLoadingFailed(FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(Bitmap bitmap) {

                    }

                    @Override
                    public void onLoadingCancelled() {

                    }
                });

                json_membal();
                jsonotp(MemCode, CategoryName, MemberName, response.getEmailID());

            }
            //  closeProgressDialog();

            // progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            showSnackAlert(title, getString(R.string.dialog_message_internet));
            json_memdetail(false);
        } else
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    //json of account detail

    void json_memdetail(boolean b) {
        if (b) {
//        progressDialog.setMessage("Loading details..");
//        progressDialog.setCancelable(true);
//        progressDialog.show();
            showProgressDialog("Loading details..");
        }
        String url = Baseurl_member + "GetMemberDetails?memcode=" + sharedPreference.getString("MemCode", "");

        Log.d("urll", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                Singleton.getinstance().saveValue(getApplicationContext(), "account_details_data", response.toString());
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(), "account_details_data"));
                AccountDetailsBean mResponseObject = new Gson().fromJson(response.toString(), AccountDetailsBean.class);
                setData(mResponseObject);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                closeProgressDialog();
                //  Toast.makeText(AccountDetails.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


//json of memberbalance

    void json_membal() {
        String url = Baseurl_member + "GetMemberBalance?memcode=" + sharedPreference.getString("MemCode", "");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                Singleton.getinstance().saveValue(getApplicationContext(), "account_balance_data", response.toString());
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(), "account_balance_data"));
                MemberBalancePojo mResponseObject = new Gson().fromJson(response.toString(), MemberBalancePojo.class);
                setBalanceData(mResponseObject);
                closeProgressDialog();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                closeProgressDialog();
                //   Toast.makeText(AccountDetails.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

    private void setBalanceData(MemberBalancePojo response) {
        try {
            Boolean status = response.getStatus();
            String ErrMsg = response.getErrMsg();
            Log.d("status", status.toString());
            if (status.equals(true)) {
                String Balance = response.getBalance().toString();
                if (Balance.charAt(0) == '-') {
                    memBalance = "Cr. " + Balance.substring(1, Balance.length());
                    balance.setText("Balance:" + memBalance);
                } else {
                    memBalance = "Dr. " + Balance;
                    payNowBalance=Balance;
                    balance.setText("Balance:" + memBalance);
                }
            } else {
                Toast.makeText(AccountDetails.this, "" + ErrMsg, Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void jsonotp(String memcode, String categoryName, String memName, String emailId) {

        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        Log.d("phn_num11", sharedPreferences1.getString("phn", "").toString().trim());
        String url = Baseurl + "register.php?phone_number=" + sharedPreferences1.getString("phn", "") + "&device_id=" + sharedPreferences1.getString("tokenn", "") + "&android_id=" + sharedPreferences1.getString("android_id", "") + "&member_code=" + memcode + "&member_status=1&category=" + URLEncoder.encode(categoryName) + "&emailid=" + URLEncoder.encode(emailId) + "&name=" + URLEncoder.encode(memName);
        Log.d("urll", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response2", response.toString());
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d("statusAccountdetails", status);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                closeProgressDialog();
                //   Toast.makeText(AccountDetails.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AccountDetails.this, CCBActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }
}
