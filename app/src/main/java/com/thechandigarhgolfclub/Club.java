package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter;
import com.thechandigarhgolfclub.Utils.GalleryAdapter1;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.thechandigarhgolfclub.Utils.SlideshowDialogFragment;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.ClubPojo;
import com.thechandigarhgolfclub.model.CoursePojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;


public class Club extends Activity implements Iconstant, ConnectivityReceiveListener {
    private static final long RIPPLE_DURATION = 250;

    RelativeLayout toolbar;
    ArrayList<String> image_list;
    FrameLayout root;
    View guillotineMenu;
    ImageView logout;
    View contentHamburger;
    private TextView title;
    SharedPreferences sharedPreference;
    SharedPreferences.Editor editor;
    WebView text_club;
    //private GridView gridView;
    private ProgressDialog progressDialog;
    private ImageView icon_app;
    ScrollView scrollview;
    private ImageView notification;
    private BadgeView badgeView;
    private RecyclerView recyclerView;
    private ArrayList<Image> images;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Club Activity")
                .build());
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        // Singleton.getinstance().saveValue(getApplicationContext(), "club_data","");

        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        text_club = (WebView) findViewById(R.id.text_club);
        // gridView = (GridView) findViewById(R.id.gridview);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        scrollview = (ScrollView) findViewById(R.id.scrollview);
        image_list = new ArrayList<String>();
        icon_app = (ImageView) findViewById(R.id.icon_app);
        logout = (ImageView) findViewById(R.id.logout);

        title.setText("Club");
        // gridView.setFocusable(false);
        images = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Club.this);
        editor = sharedPreference.edit();
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "club_data"))) {
            json_club(true);
        } else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "club_data");
            Log.e("plain", object);
            ClubPojo mResponseObject1 = new Gson().fromJson(object, ClubPojo.class);
            setData(mResponseObject1);
            json_club(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }


        if (!AppController.connection) {
            showSnackAlert(text_club, getString(R.string.dialog_message_no_internet));
        }


        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Club.this, notification);
        //noti_count();

        if (AppController.connection) {
            Singleton.getinstance().noti_count(Club.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }

        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, Notification_list.class);
                startActivity(intent);
                //      finish();
            }
        });

        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, About.class);
                startActivity(intent);
                finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, Course.class);
                startActivity(intent);
                finish();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, Club.class);
                startActivity(intent);
                finish();
            }
        });
        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });
        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Club.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppController.connection) {
                    Singleton.getinstance().logout(Club.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
    }

    private void setData(ClubPojo mResponseObject) {
        images.clear();

        try {
            Log.e("mresponse", mResponseObject.toString());
            String status = mResponseObject.getStatus();
            if (status.equalsIgnoreCase("true")) {
                Spanned sp = Html.fromHtml(mResponseObject.getDescription());
                String html = "<html><body style=text-align:justify>" + Html.toHtml(sp) + "</body></html>";
                text_club.setBackgroundColor(getResources().getColor(R.color.background_textview));
                text_club.getSettings().setJavaScriptEnabled(true);
                text_club.loadDataWithBaseURL(null, html, "text/html; charset=utf-8", "UTF-8", null);
                for (int i = 0; i < mResponseObject.getImages().size(); i++) {
                    Image image = new Image();
                    image.setMedium(mResponseObject.getImages().get(i).toString());
                    images.add(image);
                }
                // mAdapter = new GalleryAdapter(getApplicationContext(), images);
                recyclerView.setAdapter(new GalleryAdapter1(getApplicationContext(), images));
                scrollview.fullScroll(ScrollView.FOCUS_UP);
            } else {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "club_data");
                Log.e("plain", object);
                ClubPojo mResponseObject1 = new Gson().fromJson(object, ClubPojo.class);
                setData(mResponseObject1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }


    @Override
    public void onBackPressed() {
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setClosedOnStart(true)
                .build();
        finish();
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            json_club(true);
            Singleton.getinstance().noti_count(Club.this, badgeView);
            showSnackAlert(title, getString(R.string.dialog_message_internet));
        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }


    //json of club

    void json_club(boolean b) {
        if (b) {
            showProgressDialog(getString(R.string.club_loading));

        }
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        String url = Baseurl + "club.php";
        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res", response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text", plainText);
                Singleton.getinstance().saveValue(getApplicationContext(), "club_data", plainText);
                ClubPojo mResponseObject1 = new Gson().fromJson(plainText, ClubPojo.class);
                setData(mResponseObject1);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "club_data");
                Log.e("plain", object);
                ClubPojo mResponseObject1 = new Gson().fromJson(object, ClubPojo.class);
                setData(mResponseObject1);
                closeProgressDialog();

            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);


    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }
}

   /* public class ImageAdapter extends BaseAdapter {
        ImageLoader imageLoader;
        private Animator mCurrentAnimator;
        DisplayImageOptions options;
        private int mShortAnimationDuration = 1000;

        private Context mContext;
        ArrayList<String> image_list;

        public ImageAdapter(Context c, ArrayList<String> image_list) {
            mContext = c;
            this.image_list = image_list;
        }

        public int getCount() {
            return image_list.size();
        }

        public Object getItem(int position) {
            return image_list.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {


            LayoutInflater inflater = LayoutInflater.from(Club.this);
            convertView = inflater.inflate(R.layout.row_gallery, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
            final ProgressBar pbHeaderProgress = (ProgressBar) convertView.findViewById(R.id.pbHeaderProgress);

            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.loading).cacheOnDisc().cacheInMemory().build();
//            DisplayImageOptions.Builder options1 = new DisplayImageOptions.Builder().cacheInMemory(true)
//                    .cacheOnDisc(true).resetViewBeforeLoading(true);
//                    .showImageForEmptyUri(R.mipmap.paceholder)
//                    .showImageOnFail(R.mipmap.error_page_logo);
            //  imageLoader.displayImage(image_list.get(position), imageView, options);
            imageLoader.displayImage(image_list.get(position), imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted() {
                    pbHeaderProgress.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingFailed(FailReason failReason) {
                    pbHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(Bitmap bitmap) {
                    pbHeaderProgress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled() {
                    pbHeaderProgress.setVisibility(View.GONE);
                }
            });


            //imageView.setBackground(getDrawable(mThumbIds[position]));
            imageView.setTag(image_list.get(position));

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                    final Dialog settingsDialog = new Dialog(Club.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    settingsDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.gg
                            , null));
                    ImageView imageView1 = (ImageView) settingsDialog.findViewById(R.id.img);
                    ImageView back_btn = (ImageView) settingsDialog.findViewById(R.id.back_btn);

                    ScaleAnimation anim = new ScaleAnimation(1f, 1f, 1f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    anim.setDuration(500);
                    anim.setFillAfter(true);
                    imageView1.startAnimation(anim);
                    imageLoader.displayImage(image_list.get(position), imageView1, options);
                    settingsDialog.show();
                    back_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            settingsDialog.dismiss();
                        }
                    });


                }
            });
            return convertView;
        }


        private void zoomImageFromThumb(final View thumbView, int imageResId) {
            // If there's an animation in progress, cancel it immediately and proceed with this one.
            if (mCurrentAnimator != null) {
                mCurrentAnimator.cancel();
            }
            // Load the high-resolution "zoomed-in" image.
            final ImageView expandedImageView = (ImageView) ((Activity) mContext)
                    .findViewById(R.id.expanded_image);
            // Load the high-resolution "zoomed-in" image.

            expandedImageView.setImageResource(imageResId);

            // Calculate the starting and ending bounds for the zoomed-in image. This step
            // involves lots of math. Yay, math.
            final Rect startBounds = new Rect();
            final Rect finalBounds = new Rect();
            final Point globalOffset = new Point();

            // The start bounds are the global visible rectangle of the thumbnail, and the
            // final bounds are the global visible rectangle of the container view. Also
            // set the container view's offset as the origin for the bounds, since that's
            // the origin for the positioning animation properties (X, Y).
            thumbView.getGlobalVisibleRect(startBounds);
            findViewById(R.id.container).getGlobalVisibleRect(finalBounds, globalOffset);
            startBounds.offset(-globalOffset.x, -globalOffset.y);
            finalBounds.offset(-globalOffset.x, -globalOffset.y);


            // Adjust the start bounds to be the same aspect ratio as the final bounds using the
            // "center crop" technique. This prevents undesirable stretching during the animation.
            // Also calculate the start scaling factor (the end scaling factor is always 1.0).
            float startScale;
            if ((float) finalBounds.width() / finalBounds.height()
                    > (float) startBounds.width() / startBounds.height()) {
                // Extend start bounds horizontally
                startScale = (float) startBounds.height() / finalBounds.height();
                float startWidth = startScale * finalBounds.width();
                float deltaWidth = (startWidth - startBounds.width()) / 2;
                startBounds.left -= deltaWidth;
                startBounds.right += deltaWidth;
            } else {
                // Extend start bounds vertically
                startScale = (float) startBounds.width() / finalBounds.width();
                float startHeight = startScale * finalBounds.height();
                float deltaHeight = (startHeight - startBounds.height()) / 2;
                startBounds.top -= deltaHeight;
                startBounds.bottom += deltaHeight;
            }

            // Hide the thumbnail and show the zoomed-in view. When the animation begins,
            // it will position the zoomed-in view in the place of the thumbnail.
            thumbView.setAlpha(0f);
            expandedImageView.setVisibility(View.VISIBLE);

            // Set the pivot point for SCALE_X and SCALE_Y transformations to the top-left corner of
            // the zoomed-in view (the default is the center of the view).
            expandedImageView.setPivotX(0f);
            expandedImageView.setPivotY(0f);

            // Construct and run the parallel animation of the four translation and scale properties
            // (X, Y, SCALE_X, and SCALE_Y).
            AnimatorSet set = new AnimatorSet();
            set
                    .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left,
                            finalBounds.left))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top,
                            finalBounds.top))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale, 1f))
                    .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f));
            set.setDuration(mShortAnimationDuration);
            set.setInterpolator(new DecelerateInterpolator());
            set.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mCurrentAnimator = null;
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    mCurrentAnimator = null;
                }
            });
            set.start();
            mCurrentAnimator = set;

            // Upon clicking the zoomed-in image, it should zoom back down to the original bounds
            // and show the thumbnail instead of the expanded image.
            final float startScaleFinal = startScale;
            expandedImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mCurrentAnimator != null) {
                        mCurrentAnimator.cancel();
                    }

                    // Animate the four positioning/sizing properties in parallel, back to their
                    // original values.
                    AnimatorSet set = new AnimatorSet();
                    set
                            .play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left))
                            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, View.SCALE_X, startScaleFinal))
                            .with(ObjectAnimator
                                    .ofFloat(expandedImageView, View.SCALE_Y, startScaleFinal));
                    set.setDuration(mShortAnimationDuration);
                    set.setInterpolator(new DecelerateInterpolator());
                    set.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            thumbView.setAlpha(1f);
                            expandedImageView.setVisibility(View.GONE);
                            mCurrentAnimator = null;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                            thumbView.setAlpha(1f);
                            expandedImageView.setVisibility(View.GONE);
                            mCurrentAnimator = null;
                        }
                    });
                    set.start();
                    mCurrentAnimator = set;
                }
            });
        }


    }

*/

