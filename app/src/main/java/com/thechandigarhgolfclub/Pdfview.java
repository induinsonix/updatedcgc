package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.thechandigarhgolfclub.Utils.Singleton;

/**
 * Created by insonix on 28/10/16.
 */
public class Pdfview extends Activity {
    String url="";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
        try{
            url=getIntent().getStringExtra("url");
        }
        catch (Exception e){

        }
        WebView webView=new WebView(Pdfview.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        //---you need this to prevent the webview from
        // launching another browser when a url
        // redirection occurs---
        progressDialog=new ProgressDialog(Pdfview.this);
        progressDialog.setMessage("Loading");
        progressDialog.show();
        webView.setWebViewClient(new Callback());


    //    String pdfURL = "http://www.chandigarhgolfclub.in/Uploads/Editor/letter_president.pdf";
        if (Singleton.getinstance().isNetworkAvailable(Pdfview.this)){
            webView.loadUrl(
                    "http://docs.google.com/gview?embedded=true&url=" + url);
            progressDialog.dismiss();
        }
       else{
            Singleton.getinstance().dialog_internetconnection(Pdfview.this);
        }

        setContentView(webView);
    }
}

class Callback extends WebViewClient {
   @Override
   public boolean shouldOverrideUrlLoading(
           WebView view, String url) {
       return(false);
   }
}
