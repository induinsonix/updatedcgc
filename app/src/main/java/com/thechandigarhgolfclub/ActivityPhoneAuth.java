package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.firebase.digitsmigrationhelpers.AuthMigrator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.thechandigarhgolfclub.Login.Login_screen;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.MySharedPreferences;
import com.thechandigarhgolfclub.Utils.Singleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import static io.fabric.sdk.android.Fabric.TAG;

/**
 * Created by root on 15/6/17.
 */

public class ActivityPhoneAuth extends Activity implements Iconstant {

    EditText inputPhone;
    TextInputLayout inputLayoutPhoneNo;
    ImageView btnSubmit;
    EditText editOtp;
    RelativeLayout linearPhn;
    LinearLayout linearOtp;
    TextView textOtp;
    TextView textPhn;
    Button btnResend;

    MySharedPreferences mySharedPreferences;
    SharedPreferences sharedPreference, sharedPreferences1;

    // private DigitsAuthButton phoneButton;
    private SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    private String android_id;
    private SharedPreferences sharedPreferences11;


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private PhoneAuthCredential credential1;
    private FirebaseAuth mAuth;
    private String verifyId1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phn_auth);
        inputPhone = (EditText) findViewById(R.id.input_Phone);
        inputLayoutPhoneNo = (TextInputLayout) findViewById(R.id.input_layout_phone_no);
        btnSubmit = (ImageView) findViewById(R.id.btn_submit);
        editOtp = (EditText) findViewById(R.id.edit_otp);
        linearPhn = (RelativeLayout) findViewById(R.id.linear_phn);
        linearOtp = (LinearLayout) findViewById(R.id.linear_otp);
        textOtp = (TextView) findViewById(R.id.text_otp);
        textPhn = (TextView) findViewById(R.id.text_phn);
        btnResend = (Button) findViewById(R.id.btn_resend);
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(ActivityPhoneAuth.this);
        mySharedPreferences = new MySharedPreferences(getApplicationContext());
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        editor = sharedPreferences1.edit();
        btnResend.setEnabled(false);
        android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("android", android_id);
        editor.putString("android_id", android_id);
        editor.commit();

//        sharedPreference = PreferenceManager.getDefaultSharedPreferences(SplashScreen.this);
//        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
//        sharedPreferences1.edit().clear();
//        sharedPreference.edit().clear();
//        sharedPreference.edit().commit();
//        sharedPreferences1.edit().commit();
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(ActivityPhoneAuth.this);

        getPhnVerify();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(inputPhone.getText().toString())) {
                    Singleton.getinstance().showProgressDialog("Loading",ActivityPhoneAuth.this);
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(inputPhone.getText().toString(), 60, TimeUnit.MICROSECONDS, ActivityPhoneAuth.this, mCallbacks);
                    textOtp.setText(getString(R.string.otp_text, inputPhone.getText().toString()));

                    AuthMigrator.getInstance().migrate(true).addOnSuccessListener(ActivityPhoneAuth.this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
                            if (u != null) {
                                // Either a user was already logged in or token exchange succeeded
                                Log.d("MyApp", "Digits id preserved:" + u.getUid());
                                Log.d("MyApp", "Digits phone number preserved: " + u.getPhoneNumber());
                            } else {
                                Log.d(TAG, "onSuccess: nulll");
                                // No tokens were found to exchange and no Firebase user logged in.
                            }

                        }
                    }).addOnFailureListener(ActivityPhoneAuth.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d(TAG, "onFailure: " + e);
                        }
                    });
//                    AuthMigrator.getInstance().migrate(true).addOnSuccessListener(this,
//                            new OnSuccessListener() {
//                                @Override
//                                public void onSuccess(Void authResult) {
//                                    FirebaseUser u = FirebaseAuth.getInstance().getCurrentUser();
//                                    if (u != null) {
//                                        // Either a user was already logged in or token exchange succeeded
//                                        Log.d("MyApp", "Digits id preserved:" + u.getUid());
//                                        Log.d("MyApp", "Digits phone number preserved: " + u.getPhoneNumber());
//                                    } else {
//                                        // No tokens were found to exchange and no Firebase user logged in.
//                                    }
//                                }
//                            }).addOnFailureListener(ActivityPhoneAuth.this,
//                            new OnFailureListener() {
//                                @Override
//                                public void onFailure(@NonNull Exception e) {
//                                    // Error migrating Digits token
//                                }
//                            });

                } else {
                    inputPhone.setError(getString(R.string.err_phn_number));
                }

            }
        });

        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnResend.setEnabled(false);
                btnResend.setTextColor(getResources().getColor(R.color.colorAccent));
                PhoneAuthProvider.getInstance().verifyPhoneNumber(Singleton.getinstance().getValue(ActivityPhoneAuth.this, PHONENUMBER), 60, TimeUnit.MICROSECONDS, ActivityPhoneAuth.this, mCallbacks);

            }
        });
    }


    void getPhnVerify() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                credential1 = credential;
                mAuth.signInWithCredential(credential)
                        .addOnCompleteListener(ActivityPhoneAuth.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Singleton.getinstance().closeProgressDialog();
                                if (task.isSuccessful()) {
                                    FirebaseUser user = task.getResult().getUser();
                                    Singleton.getinstance().saveValue(ActivityPhoneAuth.this, PHONENUMBER, user.getPhoneNumber());
                                    jsonotp(android_id, user.getPhoneNumber());

                                } else {
                                    // Sign in failed, display a message and update the UI
                                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                        // The verification code entered was invalid

                                    }
                                }
                            }
                        });

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Singleton.getinstance().closeProgressDialog();
                if (inputPhone != null) {
                    inputPhone.requestFocus();
                 //   inputPhone.setError(getString(R.string.err_phn_number));
                    inputPhone.setError(e.getLocalizedMessage());
                }
            }

            @Override
            public void onCodeSent(String verifyId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(verifyId, forceResendingToken);
                verifyId1 = verifyId;
                linearOtp.setVisibility(View.VISIBLE);
                linearPhn.setVisibility(View.GONE);
                Singleton.getinstance().closeProgressDialog();

                textWatcher();
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                Singleton.getinstance().closeProgressDialog();
                if (btnResend != null) {
                    btnResend.setEnabled(true);
                    btnResend.setTextColor(getResources().getColor(R.color.colorWhite));
                }

            }
        };


    }

    void textWatcher() {

        editOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    Log.d(TAG, "afterTextChanged: " + editable);
                    if (editable.length() == 6 && !TextUtils.isEmpty(verifyId1)) {
                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verifyId1, editable.toString());
                        mAuth.signInWithCredential(credential)
                                .addOnCompleteListener(ActivityPhoneAuth.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "signInWithCredential:success");

                                            FirebaseUser user = task.getResult().getUser();
                                            Log.d(TAG, "onComplete: " + user.getPhoneNumber());
                                            jsonotp(android_id, user.getPhoneNumber());
                                            Singleton.getinstance().saveValue(ActivityPhoneAuth.this, PHONENUMBER, user.getPhoneNumber());
                                        } else {

                                            // Sign in failed, display a message and update the UI
                                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                                // The verification code entered was invalid
                                                if (editOtp != null) {
                                                    editOtp.requestFocus();
                                                    editOtp.setError(getString(R.string.err_otp));

                                                }
                                            }
                                        }
                                    }
                                });

                    }

                }
            }
        });


    }

    void jsonotp(String android_id, final String phoneNumber) {
        progressDialog.setMessage("Creating Profile...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        sharedPreferences11 = getSharedPreferences("neverdel", Context.MODE_PRIVATE);
        //  Log.d("urll", sharedPreferences.getString("token", ""));
        String url = Baseurl + "register.php?phone_number=" + phoneNumber + "&device_id=" + sharedPreferences11.getString("tokenn", "") + "&android_id=" + android_id + "&member_code=&member_status=0&category=&email=&name=";
        Log.d("urll", url);
        //Log.d("sharedPre", sharedPreferences.getString("tokennn", ""));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response2", response.toString());
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d("status", status);
                    if (status.equals("true")) {
                        progressDialog.dismiss();
                        editor.putString("phn_reg", "1");
                        editor.putString("phn", phoneNumber.toString());
                        editor.commit();
                        Intent intent = new Intent(getApplicationContext(), Login_screen.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(ActivityPhoneAuth.this, "" + message, Toast.LENGTH_LONG).show();

                    }
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                progressDialog.dismiss();
                Toast.makeText(ActivityPhoneAuth.this, "Unable to create Profile.Please Try again.", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                startActivity(intent);
                finish();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
//                9000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }

}
