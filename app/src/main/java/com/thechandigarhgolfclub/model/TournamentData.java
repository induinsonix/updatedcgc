
package com.thechandigarhgolfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TournamentData {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

}
