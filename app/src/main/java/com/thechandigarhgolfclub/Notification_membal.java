package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.GalleryAdapter;
import com.thechandigarhgolfclub.Utils.GalleryAdapter1;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Image;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.Utils.SlideshowDialogFragment;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


public class Notification_membal extends Activity implements Iconstant, ConnectivityReceiveListener {
    private static final long RIPPLE_DURATION = 250;
    ProgressDialog progressDialog;
    RelativeLayout toolbar;
    FrameLayout root;
    View contentHamburger;
    private TextView title, title_notify;
    TextView msg;
    private ImageView icon_app;
   // ImageView image;
    SharedPreferences sharedPreferences;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ProgressBar pbHeaderProgress;
    private ImageView notification;
    // private BadgeView badgeView;
    private ImageView logout;
    RecyclerView recyclerViewImages;
    private SharedPreferences.Editor editor;
    View guillotineMenu;
    private ArrayList<Image> images;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
  //      image = (ImageView) findViewById(R.id.image);
        images=new ArrayList<>();
        recyclerViewImages = (RecyclerView) findViewById(R.id.recycler_view_image);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        msg = (TextView) findViewById(R.id.msg);
        title_notify = (TextView) findViewById(R.id.title_notify);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Notification_membal.this);
        logout = (ImageView) findViewById(R.id.logout);
        editor = sharedPreferences.edit();
        Singleton.getinstance().logout_chek(sharedPreferences, logout);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerViewImages.setLayoutManager(mLayoutManager);
        recyclerViewImages.setItemAnimator(new DefaultItemAnimator());
        recyclerViewImages.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getApplicationContext(), recyclerViewImages, new GalleryAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("images", images);
                bundle.putInt("position", position);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                newFragment.setArguments(bundle);
                newFragment.show(ft, "slideshow");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Notification_membal.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        notification = (ImageView) findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        pbHeaderProgress = (ProgressBar) findViewById(R.id.pbHeaderProgress);

        try {
            if (AppController.connection) {
                noti_update(getIntent().getStringExtra("id"));
            } else {
                showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
            }

        } catch (Exception e) {

        }

        try {
            title.setText("Notification");
            title_notify.setText(getIntent().getStringExtra("title"));
Log.d("hyelklll",getIntent().getStringExtra("img"));
            if (!getIntent().getStringExtra("img").equals("null")&& !getIntent().getStringExtra("img").equals("")) {
                String[] imageList = getIntent().getStringExtra("img").split(",");
               // Log.d("imag", String.valueOf(imageList.length));
                if (getIntent().getStringExtra("img").equals("NULL")) {

                    recyclerViewImages.setVisibility(View.GONE);
                } else {

                    recyclerViewImages.setVisibility(View.VISIBLE);
                }

//                imageLoader = ImageLoader.getInstance();
//                imageLoader.init(ImageLoaderConfiguration.createDefault(Notification_membal.this));
//                options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
//                imageLoader.displayImage(getIntent().getStringExtra("img"), image, options);
                if (imageList.length>0){
                    for (int i = 0; i < imageList.length; i++) {
                        Image image = new Image();
                        image.setMedium(imageList[i].trim());
                        images.add(image);

                    }

                    recyclerViewImages.setAdapter(new GalleryAdapter1(getApplicationContext(), images));
                }

                msg.setText(getIntent().getStringExtra("message"));

            } else {
                msg.setText(getIntent().getStringExtra("message"));
            }
        } catch (Exception e) {
            Log.d("exception", String.valueOf(e));
        }

        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, About.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, Course.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, Tournaments.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, Club.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, Gallery.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notification_membal.this, News_events.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
    }


    //json of notification_update
    void noti_update(String id) {
      //  showProgressDialog("Loading");
        String url = Baseurl + "badge.php?id=" + id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("true")) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            showSnackAlert(title, getString(R.string.dialog_message_internet));
        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getStringExtra("notification").equals("1")) {
            Intent intent = new Intent(Notification_membal.this, CCBActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(Notification_membal.this, Notification_list.class);
            startActivity(intent);
            finish();
        }
    }

}
