package com.thechandigarhgolfclub.Login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.thechandigarhgolfclub.CCBActivity;
import com.thechandigarhgolfclub.AccountDetails;
import com.thechandigarhgolfclub.News_events;
import com.thechandigarhgolfclub.R;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.MySharedPreferences;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;

public class Login_screen extends Activity implements Iconstant, Animation.AnimationListener, ConnectivityReceiveListener {
    TextInputLayout input_layout_phn, input_layout_password;
    EditText username, password, edit_phn;
    CheckBox rememberme;
    Button memeber_login;
    SharedPreferences sharedPreferences;
    TextView skip, textMemberLogin, textForgot;
    LinearLayout login_lay;
    SharedPreferences.Editor editor;
    ProgressDialog progressDialog;
    MySharedPreferences mySharedPreference;
    private Animation animSideDown;
    private SharedPreferences sharedPreferences1;
    RelativeLayout relativeForgotPass;
    private SharedPreferences sharedPreferences11;
    String loginChek;
    TextInputLayout input_layout_Number, input_layout_pass;
    private String forgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        password = (EditText) findViewById(R.id.password);
        relativeForgotPass = (RelativeLayout) findViewById(R.id.relative_forgot_pass);
        input_layout_pass = (TextInputLayout) findViewById(R.id.input_layout_pass);
        input_layout_Number = (TextInputLayout) findViewById(R.id.input_layout_Number);
        username = (EditText) findViewById(R.id.username);
        edit_phn = (EditText) findViewById(R.id.edit_phn);
        memeber_login = (Button) findViewById(R.id.memeber_login);
        rememberme = (CheckBox) findViewById(R.id.rememberme);
        login_lay = (LinearLayout) findViewById(R.id.login_lay);
        skip = (TextView) findViewById(R.id.skip);
        textForgot = (TextView) findViewById(R.id.text_forgot);
        textMemberLogin = (TextView) findViewById(R.id.text_member_login);

        mySharedPreference = new MySharedPreferences(getApplicationContext());
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);
        animSideDown.setAnimationListener(Login_screen.this);
        login_lay.setVisibility(View.VISIBLE);
        login_lay.startAnimation(animSideDown);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Login_screen.this);
        editor = sharedPreferences.edit();
        loginChek = "MemberLogin";
        textForgot.setText("Forgot Password ?");
        memeber_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loginChek.equals("MemberLogin")) {
                    if (TextUtils.isEmpty(username.getText().toString().trim())) {
                        username.setError("Please Enter User Id");
                    } else if (TextUtils.isEmpty(password.getText().toString().trim())) {
                        password.setError("Please Enter password");
                    } else {

                        if (AppController.connection) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            jsonmembers();
                            editor.putString("login", "1");
                            editor.commit();
                        } else {
                            showSnackAlert(username, getString(R.string.dialog_message_no_internet));
                        }

                    }
                } else {


                    if (TextUtils.isEmpty(username.getText().toString().trim())) {
                        username.setError("Please Enter User Id");
                    } else if (TextUtils.isEmpty(edit_phn.getText().toString().trim())) {
                        password.setError("Please Enter Phone Number");
                    } else if ((("+91" + edit_phn.getText().toString().trim()).compareTo(sharedPreferences1.getString("phn", ""))) != 0) {
                        new AlertDialog.Builder(Login_screen.this)
                                .setTitle("Retrieve Password")
                                .setMessage(getResources().getString(R.string.number_nomatch_popup))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        json_forgetPass(username.getText().toString().trim(), edit_phn.getText().toString().trim());
                                    }
                                })
                                .show();
                        // password.setError("Your Phone number is not match with register mobile number in CGC App");
                    } else {
                        if (AppController.connection) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            json_forgetPass(username.getText().toString().trim(), edit_phn.getText().toString().trim());
//                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Login_screen.this);
//                            final AlertDialog alertDialog = dialogBuilder.create();
//                            LayoutInflater inflater = getLayoutInflater();
//                            alertDialog.setContentView(inflater.inflate(R.layout.dialog_get_password, null));
//                            TextView dialogPassword = (TextView) alertDialog.findViewById(R.id.text_password);
//                            Button dialogProceedLogin = (Button)alertDialog.findViewById(R.id.btn_login);
//                            Button dialogCancel = (Button) alertDialog.findViewById(R.id.btn_cancel);
//                            alertDialog.show();
//                            dialogPassword.setText(forgotPassword);
//                            dialogProceedLogin.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    alertDialog.cancel();
//                                    loginChek = "MemberLogin";
//                                    input_layout_pass.setVisibility(View.VISIBLE);
//                                    input_layout_Number.setVisibility(View.GONE);
//                                    ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
//                                    cm.setText(forgotPassword);
//
//                                }
//                            });


//                            new AlertDialog.Builder(Login_screen.this)
//                                    .setTitle("Your Password")
//                                    .setMessage(forgotPassword)
//                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                      //      Log.d("passdd",forgotPassword);
//
//                                            // continue with delete
//                                        }
//                                    })
//
//                                    //.setIcon(android.R.drawable.ic_dialog_alert)
//                                    .show();
                        } else {
                            showSnackAlert(username, getString(R.string.dialog_message_no_internet));
                        }

                    }


                }


            }
        });


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("login", "1");
                editor.commit();
                Intent intent = new Intent(Login_screen.this, CCBActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });

        relativeForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loginChek.equals("MemberLogin")) {
                    memeber_login.setText("Retrieve Password");
                    textForgot.setText(getResources().getString(R.string.member));
                    input_layout_pass.setVisibility(View.GONE);
                    input_layout_Number.setVisibility(View.VISIBLE);
                    loginChek = "ForgotPass";
                    textMemberLogin.setText("Retrieve Password");
                    new AlertDialog.Builder(Login_screen.this)
                            .setTitle("Retrieve Password")
                            .setMessage(getResources().getString(R.string.retrieve_popup))
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete

                                }
                            })
                            .show();
                } else {
                    memeber_login.setText(getResources().getString(R.string.member));
                    textForgot.setText("Forgot Password ?");
                    input_layout_pass.setVisibility(View.VISIBLE);
                    input_layout_Number.setVisibility(View.GONE);
                    loginChek = "MemberLogin";
                    textMemberLogin.setText(getResources().getString(R.string.member));

                }


            }
        });
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            // jsonmembers();
            showSnackAlert(username, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(username, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    //json of login
    void jsonmembers() {
        showProgressDialog(getString(R.string.login_loading));
        final String[] ErrMsg = {""};
        String url = Baseurl_member + "CheckLoginStatus?custcode=" + URLEncoder.encode(username.getText().toString()) + "&custpwd=" + password.getText().toString();
        Log.d("urlLogin", url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Boolean status = response.getBoolean("Status");
                    ErrMsg[0] = response.getString("ErrMsg");
                    if (status.equals(true)) {
                        String MemCode = response.getString("MemCode");
                        json_memdetail(MemCode);
                        editor.putString("members_detail", "1");
                        editor.putString("MemCode", MemCode);
                        editor.commit();


                    } else {
                        closeProgressDialog();
                        Toast.makeText(Login_screen.this, " " + ErrMsg[0], Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);


    }
    //json of member detail

    void json_memdetail(String memcode) {
//        progressDialog.setMessage("Loading..");
//        progressDialog.show();

        String url = Baseurl_member + "GetMemberDetails?memcode=" + memcode;


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                try {
                    Boolean status = response.getBoolean("Status");
                    Log.d("status", status.toString());
                    if (status.equals(true)) {
                        String MemCode = response.getString("MemCode");
                        Log.d("MemCode", MemCode.toString());

                        String BirthDate = response.getString("BirthDate");
                        String MemberFirstName = response.getString("MemberFirstName");
                        String MemberMiddleName = response.getString("MemberMiddleName");
                        String MemberLastName = response.getString("MemberLastName");
                        String FatherName = response.getString("FatherName");
                        String CategoryName = response.getString("CategoryName");
                        String CorporateName = response.getString("CorporateName");
                        String Address1 = response.getString("Address1");
                        String Address2 = response.getString("Address2");
                        String Address3 = response.getString("Address3");
                        String CityName = response.getString("CityName");
                        String StateName = response.getString("StateName");
                        String Phone1 = response.getString("Phone1");
                        String Phone2 = response.getString("Phone2");

                        String MobileNo = response.getString("MobileNo");
                        String EmailID = response.getString("EmailID");
                        String CreditLimit = response.getString("CreditLimit");
                        String MemberName = MemberFirstName + " " + MemberMiddleName + " " + MemberLastName;
                        jsonotp(MemCode, CategoryName, MemberName, EmailID);

                    }
//closeProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void jsonotp(String memcode, String categoryName, String memName, String emailId) {

        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        sharedPreferences11 = getSharedPreferences("neverdel", Context.MODE_PRIVATE);
        Log.d("phn_num11", sharedPreferences1.getString("phn", "").toString().trim());
        String url = Baseurl + "register.php?phone_number=" + sharedPreferences1.getString("phn", "") + "&device_id=" + sharedPreferences11.getString("tokenn", "") + "&android_id=" + sharedPreferences1.getString("android_id", "") + "&member_code=" + memcode + "&member_status=1&category=" + URLEncoder.encode(categoryName) + "&emailid=" + URLEncoder.encode(emailId) + "&name=" + URLEncoder.encode(memName);

        Log.d("urll", url);
        Log.d("sharedPre", sharedPreferences.getString("tokennn", ""));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("response2", response.toString());
                try {
                    String status = response.getString("status");
                    String message = response.getString("message");
                    Log.d("status", status);
                    if (status.equals("true")) {
                        Intent intent = new Intent(Login_screen.this, AccountDetails.class);
                        startActivity(intent);
                        finish();
                        closeProgressDialog();
                    } else {
                        closeProgressDialog();
                        Toast.makeText(Login_screen.this, "" + message, Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }

    void json_forgetPass(String memId, String phoneNumber) {
        Log.d("call", "forgot" + phoneNumber);
        showProgressDialog("Retrive Password");
        String url = Baseurl_member + "ReSendLoginPassword?memcode=" + memId + "&mobileno=" + phoneNumber;
        Log.d("url", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", String.valueOf(response));

                try {
                    Boolean status = response.getBoolean("Status");
                    Log.d("staus", String.valueOf(status));
                    if (status == true) {
                        forgotPassword = response.getString("Pwd");
                        Log.d("pas", forgotPassword);
                        final Dialog settingsDialog = new Dialog(Login_screen.this, android.R.style.Theme_Holo_Dialog_NoActionBar);
                        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.dialog_get_password
                                , null));

                        //   settingsDialog.getWindow().setLayout(width, height);
                        settingsDialog.show();
                        TextView dialogPassword = (TextView) settingsDialog.findViewById(R.id.text_password);
                        //    TextView btnCopy = (TextView) settingsDialog.findViewById(R.id.btn_copy);
                        Button dialogProceedLogin = (Button) settingsDialog.findViewById(R.id.btn_login);
                        final Button btn_copy = (Button) settingsDialog.findViewById(R.id.btn_copy);
                        dialogPassword.setText(forgotPassword);

                        btn_copy.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //   btn_copy.setVisibility(View.GONE);
                                Toast.makeText(Login_screen.this, "Copied password to clipboard", Toast.LENGTH_LONG).show();
                            }
                        });

                        dialogProceedLogin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                settingsDialog
                                        .cancel();
                                loginChek = "MemberLogin";
                                memeber_login.setText(getResources().getString(R.string.member));
                                input_layout_pass.setVisibility(View.VISIBLE);
                                input_layout_Number.setVisibility(View.GONE);
                                textForgot.setText("Forgot Password ?");
                                textMemberLogin.setText(getResources().getString(R.string.member));
                                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                cm.setText(forgotPassword);

                            }
                        });

                    } else {
                        Toast.makeText(Login_screen.this, response.getString("ErrMsg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
            }
        });
        AppController.getInstance(Login_screen.this).addToRequestQueue(jsonObjectRequest);

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Login_screen.this, CCBActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
