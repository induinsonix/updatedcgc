
package com.thechandigarhgolfclub.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountDetailsBean {

    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("ErrMsg")
    @Expose
    private String errMsg;
    @SerializedName("MemberFirstName")
    @Expose
    private String memberFirstName;
    @SerializedName("MemberMiddleName")
    @Expose
    private String memberMiddleName;
    @SerializedName("MemberLastName")
    @Expose
    private String memberLastName;
    @SerializedName("FatherName")
    @Expose
    private String fatherName;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("CorporateName")
    @Expose
    private String corporateName;
    @SerializedName("MemCode")
    @Expose
    private String memCode;
    @SerializedName("PanNo")
    @Expose
    private String panNo;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("Address3")
    @Expose
    private String address3;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("StateName")
    @Expose
    private String stateName;
    @SerializedName("Phone1")
    @Expose
    private String phone1;
    @SerializedName("Phone2")
    @Expose
    private String phone2;
    @SerializedName("MobileNo")
    @Expose
    private String mobileNo;
    @SerializedName("EmailID")
    @Expose
    private String emailID;
    @SerializedName("BirthDate")
    @Expose
    private String birthDate;
    @SerializedName("Remarks1")
    @Expose
    private String remarks1;
    @SerializedName("Remarks2")
    @Expose
    private String remarks2;
    @SerializedName("Remarks3")
    @Expose
    private String remarks3;
    @SerializedName("CreditLimit")
    @Expose
    private String creditLimit;
    @SerializedName("IsOutStation")
    @Expose
    private Boolean isOutStation;
    @SerializedName("ImageExists")
    @Expose
    private Boolean imageExists;
    @SerializedName("ImagePath")
    @Expose
    private String imagePath;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getMemberFirstName() {
        return memberFirstName;
    }

    public void setMemberFirstName(String memberFirstName) {
        this.memberFirstName = memberFirstName;
    }

    public String getMemberMiddleName() {
        return memberMiddleName;
    }

    public void setMemberMiddleName(String memberMiddleName) {
        this.memberMiddleName = memberMiddleName;
    }

    public String getMemberLastName() {
        return memberLastName;
    }

    public void setMemberLastName(String memberLastName) {
        this.memberLastName = memberLastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getMemCode() {
        return memCode;
    }

    public void setMemCode(String memCode) {
        this.memCode = memCode;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1;
    }

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks3() {
        return remarks3;
    }

    public void setRemarks3(String remarks3) {
        this.remarks3 = remarks3;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Boolean getIsOutStation() {
        return isOutStation;
    }

    public void setIsOutStation(Boolean isOutStation) {
        this.isOutStation = isOutStation;
    }

    public Boolean getImageExists() {
        return imageExists;
    }

    public void setImageExists(Boolean imageExists) {
        this.imageExists = imageExists;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
