
package com.thechandigarhgolfclub.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LedgerDetailsPojo {

    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("ErrMsg")
    @Expose
    private String errMsg;
    @SerializedName("LedDetails")
    @Expose
    private List<LedDetail> ledDetails = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public List<LedDetail> getLedDetails() {
        return ledDetails;
    }

    public void setLedDetails(List<LedDetail> ledDetails) {
        this.ledDetails = ledDetails;
    }

}
