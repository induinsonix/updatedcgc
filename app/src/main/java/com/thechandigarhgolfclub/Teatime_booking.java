package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;


public class Teatime_booking extends Activity implements ConnectivityReceiveListener {

    private static final long RIPPLE_DURATION = 250;
    RelativeLayout toolbar;
    TextView text_join;
    ImageView icon_app;
    FrameLayout root;
    View guillotineMenu;
    View contentHamburger;
    ProgressDialog progressDialog;
    private WebView webView;
    private TextView title;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    private ImageView notification;
    private BadgeView badgeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teatime_booking);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        text_join = (TextView) findViewById(R.id.text_join);
        progressDialog = new ProgressDialog(Teatime_booking.this);
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Teatime_booking.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.getinstance().logout(Teatime_booking.this, editor);
            }
        });
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        title.setText("Teetime Booking");

        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Teatime_booking.this, notification);
        //noti_count();
        if (AppController.connection) {
            progressDialog.setMessage("Connecting ...");
            progressDialog.show();
            Singleton.getinstance().noti_count(Teatime_booking.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, Notification_list.class);
                startActivity(intent);
                //  finish();
            }
        });

        text_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Teatime_booking.this, LastMinuteGOlfActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, CCBActivity.class);
                startActivity(intent);
                finish();
            }
        });
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, About.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, Course.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                // finish();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, Tournaments.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //  finish();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, Club.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //  finish();
            }
        });
        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, Gallery.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                // finish();
            }
        });
        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Teatime_booking.this, News_events.class);
                startActivity(intent);
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setClosedOnStart(true)
                        .build();
                //finish();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();


        webView = (WebView) findViewById(R.id.webView1);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://cgc.golflan.com/");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {

            Singleton.getinstance().noti_count(Teatime_booking.this, badgeView);

            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            progressDialog.dismiss();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    @Override
    public void onBackPressed() {

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setClosedOnStart(true)
                .build();
        Intent intent = new Intent(Teatime_booking.this, CCBActivity.class);
        startActivity(intent);
        finish();
    }
}
