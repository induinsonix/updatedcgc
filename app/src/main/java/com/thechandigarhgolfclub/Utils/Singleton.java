package com.thechandigarhgolfclub.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.thechandigarhgolfclub.BadgeView;
import com.thechandigarhgolfclub.Login.Login_screen;
import com.thechandigarhgolfclub.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by insonix on 27/10/16.
 */
public class Singleton implements Iconstant {
    static Singleton singleton = new Singleton();
    private ProgressDialog progressDialog;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor ;
    public String mypreference = "mypref";

    public static Singleton getinstance() {
        return singleton;
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void dialog_internetconnection(Context context) {
        new AlertDialog.Builder(context)
                .setTitle("Internet Connection Failed")
                .setMessage("Internet is not available")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })


                .show();
    }


    public void logout_chek(SharedPreferences sharedPreferences, View view) {
        if (sharedPreferences.getString("MemCode", "").equals("")) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }

    }

    public void logout(final Activity c1, final SharedPreferences.Editor editor) {

        final SharedPreferences sharedPreferences = c1.getSharedPreferences("notdel", Context.MODE_PRIVATE);
        new AlertDialog.Builder(c1)
                .setTitle("Logout")
                .setMessage("Are you sure you want to logout app?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        if (isNetworkAvailable(c1)) {

                            json_logout(c1, sharedPreferences.getString("phn", ""), editor);
                        } else {
                            dialog_internetconnection(c1);
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.logout1)
                .show();

    }


    void json_logout(final Activity c1, String phn_num, final SharedPreferences.Editor editor) {
        //  final ProgressDialog progressDialog=new ProgressDialog(c1);
        showProgressDialog("Logging Out..", c1);
//        progressDialog.setMessage("Logging Out..");
//        progressDialog.show();
        String url = Baseurl + "logout.php?phone_number=" + phn_num;
        Log.d("abouturl", url + "phnnn");
        Log.d("phn_num", phn_num);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String plainText = Html.fromHtml(response).toString();
                try {
                    JSONObject jsonObject = new JSONObject(plainText);
                    String status = jsonObject.getString("status");

                    if (status.equalsIgnoreCase("true")) {
                        editor.clear();
                        editor.commit();
                        Intent intent = new Intent(c1, Login_screen.class);
                        c1.startActivity(intent);
                        c1.finish();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                closeProgressDialog();
                //progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(c1).addToRequestQueue(stringRequest);
    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Log.d("appcount", String.valueOf(count));
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    // json of noti count

    public void noti_count(final Activity c1, final BadgeView badgeView) {

        final SharedPreferences sharedPreferences1 = c1.getSharedPreferences("notdel", Context.MODE_PRIVATE);
//        showProgressDialog("Logging..",c1);
        String url = Baseurl + "badge_count.php?phone_number=" + sharedPreferences1.getString("phn", "");
        Log.d("url_phn", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    String status = response.getString("status");

                    if (status.equals("true")) {
                        int count = response.getInt("count");
                        Log.d("count", count + "");
                        setBadge(c1, count);
                        if (count == 0) {

                        } else {
                            badgeView.setText(count + "");
                            Log.d("counddddt", count + "");
                            badgeView.show();

                        }


                    }
                } catch (JSONException e) {
                    Log.d("eee", e + "");
                    e.printStackTrace();
                }
                //    closeProgressDialog();

                //progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressDialog.dismiss();
                //  closeProgressDialog();


            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(c1).addToRequestQueue(jsonObjectRequest);
    }

    public void showProgressDialog(String message, Context context) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    public void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    public  void saveValue(Context context, String key, String value)
    {
        preferences=context.getSharedPreferences(mypreference, 0);
        editor=preferences.edit();
        editor.putString(key,value);
        editor.commit();
    }
    public String getValue(Context context, String key)

    {
        preferences=context.getSharedPreferences(mypreference, 0);
        String value=preferences.getString(key,"");
        return value;
    }

}





