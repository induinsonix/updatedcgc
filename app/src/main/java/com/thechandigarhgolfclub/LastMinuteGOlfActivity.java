package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.model.ClubPojo;
import com.thechandigarhgolfclub.model.CoursePojo;
import com.thechandigarhgolfclub.model.LastMinutePojo;
import com.thechandigarhgolfclub.model.Message;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by insonix on 16/1/17.
 */

public class LastMinuteGOlfActivity extends Activity implements Iconstant, ConnectivityReceiveListener {
    Button btnGo;
    ArrayList<HashMap<String, String>> mList;
    LinearLayout linear_refresh;
    TextView text_no_data;
    ListView listView;
    EditText inputTime, inputName, inputMobile;
    Calendar mcurrentTime;
    int hour, minute;
    ImageView back_arrow, img_refresh;
    ProgressBar loadingdata_progress;
    private ProgressDialog progressDialog;
    private int month, day, year;
    private JoinGameAdapter joinGameAdapter;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_minute_golf);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        inputTime = (EditText) findViewById(R.id.input_time);
        inputName = (EditText) findViewById(R.id.input_name);
        text_no_data = (TextView) findViewById(R.id.text_no_data);
        inputMobile = (EditText) findViewById(R.id.input_mobile);
        loadingdata_progress = (ProgressBar) findViewById(R.id.loadingdata_progress);
        btnGo = (Button) findViewById(R.id.btn_go);
        linear_refresh = (LinearLayout) findViewById(R.id.linear_refresh);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        img_refresh = (ImageView) findViewById(R.id.img_refresh);
        gson = new Gson();
        mList = new ArrayList<>();
        mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
        year = mcurrentTime.get(Calendar.YEAR);
        month = mcurrentTime.get(Calendar.MONTH) + 1;
        day = mcurrentTime.get(Calendar.DAY_OF_MONTH);
        if (hour>12){
            hour=hour-12;
            inputTime.setText(hour + ":" + minute);
        }
        else {
            inputTime.setText(hour + ":" + minute);
        }

        listView = (ListView) findViewById(R.id.list_item);
        linear_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (AppController.connection) {
                    loadingdata_progress.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    Animation rotation = AnimationUtils.loadAnimation(LastMinuteGOlfActivity.this, R.anim.refresh_anim);
                    rotation.setRepeatCount(Animation.INFINITE);
                    img_refresh.startAnimation(rotation);
                    jsonGetGame();
                } else {
                    showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
                }
//                if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "getgame_data"))) {
//                    if (AppController.connection) {
//                        jsonGetGame(true);
//                    } else {
//                        showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
//                    }
//                } else {
//                    String object = Singleton.getinstance().getValue(getApplicationContext(), "getgame_data");
//                    Log.e("plain", object);
//                    LastMinutePojo mResponseObject1 = new Gson().fromJson(object, LastMinutePojo.class);
//                    setData(mResponseObject1);
//                    jsonGetGame(false);
//
//                    //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
//                }
            }
        });


        // item.setActionView(img_refresh);
        inputTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                final Dialog dialog = new Dialog(LastMinuteGOlfActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);// Context, this, etc.
                dialog.setContentView(R.layout.dialog_time_picker);
               // dialog.setTitle(R.string.time);

                TimePicker tp = (TimePicker)dialog.findViewById(R.id.tp);
                Button btn_ok = (Button)dialog.findViewById(R.id.btn_ok);

                tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                    @Override
                    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay>12){
                            hourOfDay=hourOfDay-12;
                            inputTime.setText(hourOfDay + ":" + minute);
                        }
                        else {
                            inputTime.setText(hourOfDay + ":" + minute);
                        }
                        //Display the new time to app interface

                    }
                });
                btn_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();




               /* final RangeTimePickerDialog mTimePicker;
                mTimePicker = new RangeTimePickerDialog(LastMinuteGOlfActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                    }
                }, hour, minute, false);//true = 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.setMin(hour, minute);
                mTimePicker.show();*/
                // TODO Auto-generated method stub
//                TimePickerDialog mTimePicker;
//                mTimePicker = new TimePickerDialog(LastMinuteGOlfActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                        inputTime.setText(selectedHour + ":" + selectedMinute);
////                        if (hour<selectedHour) {
////
////                            // prevent user from selecting time
////                        }
//
//
//                    }
//                }, hour, minute, true);//Yes 24 hour time
//          //      mTimePicker.getD
//
//                mTimePicker.setTitle("Select Time");
//                mTimePicker.show();

            }
        });

        if (AppController.connection) {
            jsonGetGame();
        } else {
            showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
        }

//        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "getgame_data"))) {
//            if (AppController.connection) {
//                jsonGetGame(true);
//            } else {
//                showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
//            }
//        } else {
//            String object = Singleton.getinstance().getValue(getApplicationContext(), "getgame_data");
//            Log.e("plain", object);
//            LastMinutePojo mResponseObject1 = new Gson().fromJson(object, LastMinutePojo.class);
//            setData(mResponseObject1);
//            jsonGetGame(false);
//
//            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
//        }


        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(inputName.getText().toString().trim())) {
                    inputName.setError("Enter Name");
                } else if (TextUtils.isEmpty(inputMobile.getText().toString().trim())) {
                    inputMobile.setError("Enter Mobile");
                } else if (inputMobile.getText().toString().trim().length()<10) {
                    inputMobile.setError("Enter Valid Number");
                } else {

                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    if (AppController.connection) {
                        jsonJoinGame(inputName.getText().toString().trim(), inputMobile.getText().toString().trim(), inputTime.getText().toString().trim());
                    } else {
                        showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
                    }
                }
            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    void jsonJoinGame(String name, String mobile, String time) {
        showProgressDialog("Join Game");
        String url = Baseurl + "joingame.php?phone_number=" + URLEncoder.encode(mobile) + "&name=" + URLEncoder.encode(name) + "&datetime=" + URLEncoder.encode(year + "-" + month + "-" + day + " " + time);
        Log.d("url", url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status = response.getString("status");
                    if (status.equals("true")) {
                        inputName.setText("");
                        inputMobile.setText("");
                        inputMobile.setFocusable(false);
                        inputMobile.setFocusableInTouchMode(true);
                        jsonGetGame();
                    } else {
                        loadingdata_progress.setVisibility(View.GONE);
                        closeProgressDialog();
                        Toast.makeText(LastMinuteGOlfActivity.this, response.getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {

                }
                //  closeProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
                loadingdata_progress.setVisibility(View.GONE);
            }
        });
        AppController.getInstance(LastMinuteGOlfActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    void jsonGetGame() {

        loadingdata_progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        // loadingdata_progress.setVisibility(View.VISIBLE);
        final String url = Baseurl + "getgame.php?current_time=" + URLEncoder.encode(year + "-" + month + "-" + day + " " + hour + ":" + month + ":00");
        Log.d("url", url);
        mList = new ArrayList<>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                Singleton.getinstance().saveValue(getApplicationContext(), "getgame_data", String.valueOf(response));
                LastMinutePojo mResponseObject1 = new Gson().fromJson(String.valueOf(response), LastMinutePojo.class);
                setData(mResponseObject1);
                closeProgressDialog();
//                    String status = response.getString("status");
//                    if (status.equals("true")) {
//                        Log.d("getgame", String.valueOf(response));
//                        JSONArray jsonArray = response.getJSONArray("message");
//
//
//                        for (int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//                            HashMap map = new HashMap();
//                            map.put("name", jsonObject.get("name"));
//                            map.put("phone", jsonObject.get("phone"));
//                            map.put("date_time", jsonObject.get("date_time"));
//                            mList.add(map);
//                        }
//                        listView.setVisibility(View.VISIBLE);
//                        joinGameAdapter = new JoinGameAdapter(mList);
//                        listView.setAdapter(joinGameAdapter);
//                        joinGameAdapter.notifyDataSetChanged();
//                        img_refresh.clearAnimation();
//                        closeProgressDialog();
//                    } else {
//
//                        img_refresh.clearAnimation();
//                        Toast.makeText(LastMinuteGOlfActivity.this, "Data is empty", Toast.LENGTH_LONG).show();
//                    }

                loadingdata_progress.setVisibility(View.GONE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
                Log.d("error", String.valueOf(error));
                loadingdata_progress.setVisibility(View.GONE);
            }
        });
        AppController.getInstance(LastMinuteGOlfActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private boolean weHavePermissionToReadContacts() {
        return ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadContactsPermissionFirst() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CALL_PHONE) && ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.CALL_PHONE)) {
//            Toast.makeText(this, "We need permission so you can access your External storage", Toast.LENGTH_LONG).show();
            requestForResultContactsPermission();
        } else {
            requestForResultContactsPermission();
        }
    }

    private void requestForResultContactsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE}, 123);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(LastMinuteGOlfActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(LastMinuteGOlfActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
        }
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LastMinuteGOlfActivity.this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            // json_club(true);
            showSnackAlert(inputMobile, getString(R.string.dialog_message_internet));
        } else {
            closeProgressDialog();
            showSnackAlert(listView, getString(R.string.dialog_message_no_internet));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }

    public void setData(LastMinutePojo response) {
        mList = new ArrayList<>();
        String status = response.getStatus();
        if (status.equals("true")) {
            Log.d("getgame", String.valueOf(response));
            List<Message> jsonArray = response.getMessage();
            if (!jsonArray.isEmpty()) {
                for (int i = 0; i < jsonArray.size(); i++) {
                    // JSONObject jsonObject = jsonArray.getJSONObject(i);
                    HashMap map = new HashMap();
                    map.put("name", jsonArray.get(i).getName());
                    map.put("phone", jsonArray.get(i).getPhone());
                    map.put("date_time", jsonArray.get(i).getDateTime());
                    mList.add(map);
                }
                Collections.reverse(mList);
                listView.setVisibility(View.VISIBLE);
                joinGameAdapter = new JoinGameAdapter(mList);
                listView.setAdapter(joinGameAdapter);
                joinGameAdapter.notifyDataSetChanged();
                img_refresh.clearAnimation();
            } else {
                img_refresh.clearAnimation();
                text_no_data.setVisibility(View.VISIBLE);
                //       Toast.makeText(LastMinuteGOlfActivity.this, "Data is empty", Toast.LENGTH_LONG).show();
            }

            closeProgressDialog();
        } else {


//            String object = Singleton.getinstance().getValue(getApplicationContext(), "getgame_data");
//            Log.e("plain", object);
//            LastMinutePojo mResponseObject1 = gson.fromJson(object, LastMinutePojo.class);
//            setData(mResponseObject1);
//            img_refresh.clearAnimation();
            // Toast.makeText(LastMinuteGOlfActivity.this, "Data is empty", Toast.LENGTH_LONG).show();
        }
        //  this.data = data;
    }


    class JoinGameAdapter extends BaseAdapter {
        ArrayList<HashMap<String, String>> mListAdapter;

        public JoinGameAdapter(ArrayList<HashMap<String, String>> mList) {
            mListAdapter = mList;
        }

        @Override
        public int getCount() {
            Log.d("size", String.valueOf(mListAdapter.size()));
            return mListAdapter.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = LayoutInflater.from(LastMinuteGOlfActivity.this);
            view = inflater.inflate(R.layout.tabular_row1, null);
            TextView textTime = (TextView) view.findViewById(R.id.text_time);
            TextView textName = (TextView) view.findViewById(R.id.text_name);
            TextView textMobile = (TextView) view.findViewById(R.id.text_mobile);
            TextView textSerial = (TextView) view.findViewById(R.id.text_sr);
            LinearLayout linear_layout = (LinearLayout) view.findViewById(R.id.linear_layout);

            if (i % 2 == 0)
                linear_layout.setBackgroundColor(getResources().getColor(R.color.mem_bg));
            else
                linear_layout.setBackgroundColor(getResources().getColor(R.color.btn_text));

            textTime.setText(mListAdapter.get(i).get("date_time"));
            textName.setText(mListAdapter.get(i).get("name"));
            textMobile.setText(mListAdapter.get(i).get("phone"));
            textSerial.setText((i + 1) + "");
            textMobile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (weHavePermissionToReadContacts()) {
                        // TODO: Consider calling
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mListAdapter.get(i).get("phone")));
                        startActivity(intent);
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    } else {
                        requestReadContactsPermissionFirst();
                    }

                }
            });

            return view;
        }

    }

    class RangeTimePickerDialog extends TimePickerDialog {

        private int minHour = -1;
        private int minMinute = -1;
        String chek;
        private int maxHour = 25;
        private int maxMinute = 25;

        private int currentHour = 0;
        private int currentMinute = 0;

        private Calendar calendar = Calendar.getInstance();
        private DateFormat dateFormat;


        public RangeTimePickerDialog(Context context, OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView) {
            super(context, callBack, hourOfDay, minute, is24HourView);
            currentHour = hourOfDay;
            currentMinute = minute;
            dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT);

            try {
                Class<?> superclass = getClass().getSuperclass();
                Field mTimePickerField = superclass.getDeclaredField("mTimePicker");
                mTimePickerField.setAccessible(true);
                TimePicker mTimePicker = (TimePicker) mTimePickerField.get(this);
                mTimePicker.setOnTimeChangedListener(this);
            } catch (NoSuchFieldException e) {
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            }
        }

        public void setMin(int hour, int minute) {
            minHour = hour;
            minMinute = minute;
        }

        public void setMax(int hour, int minute) {
            maxHour = hour;
            maxMinute = minute;
        }

        @Override
        public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

            Log.d("DADADADA", "onTimeChanged");

            boolean validTime = true;
            if (hourOfDay < minHour || (hourOfDay == minHour && minute < minMinute)) {
                validTime = false;
                chek = "Select Time should be greater than current time";
                dialogTime("Select Time should be greater than current time");
              //  chek = "";
            }

            if (hourOfDay > maxHour || (hourOfDay == maxHour && minute > maxMinute)) {
                validTime = false;
                //chek = "";
                dialogTime("Select Time should be greater than current time");
            }

            if (validTime) {
                chek = "";
                currentHour = hourOfDay;
                currentMinute = minute;
            }
            updateTime(currentHour, currentMinute);
            updateDialogTitle(view, currentHour, currentMinute);
        }

        private void updateDialogTitle(TimePicker timePicker, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            String title = dateFormat.format(calendar.getTime());
            inputTime.setText(hourOfDay + ":" + minute);
            setTitle(hourOfDay + ":" + minute);
        }

        void dialogTime(String msg){
            new AlertDialog.Builder(LastMinuteGOlfActivity.this)
                    .setTitle("Time Validation")
                    .setMessage(msg)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();

                        }
                    })

                    .show();
        }

    }
}
