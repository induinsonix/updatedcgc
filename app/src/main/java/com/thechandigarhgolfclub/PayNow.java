package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import com.thechandigarhgolfclub.Utils.Iconstant;

import java.net.URLEncoder;

import static com.thechandigarhgolfclub.Utils.AppController.TAG;

public class PayNow extends Activity implements Iconstant {
    ImageView back_btn;
    String mBalance, mMemCode, mCategoryName,mName;
    WebView webView;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_now);
        webView = (WebView) findViewById(R.id.webView);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        progressDialog = new ProgressDialog(this,R.style.MyTheme);

        mBalance = getIntent().getStringExtra(Balance);
        mCategoryName = getIntent().getStringExtra(CATEGORY);
        mMemCode = getIntent().getStringExtra(MEMID);
        mName = getIntent().getStringExtra(NAME);


        Log.d(TAG, "onCreate: " + mBalance + "" + mCategoryName + "" + mMemCode);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PayNow.this, AccountDetails.class);
                startActivity(intent);
                finish();
            }
        });

        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);


        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(PayNow.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressDialog.setMessage("Loading...");
                progressDialog.show();
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();

            }

        });
        Log.d(TAG, Baseurl+"bankapi/payment.php?amt=" + mBalance + "&trref=" + mMemCode + "&OrderInfo=" + URLEncoder.encode(mCategoryName)+"&memName="+URLEncoder.encode(mName));
        webView.loadUrl(Baseurl+"bankapi/payment.php?amt=" + mBalance + "&trref=" + mMemCode + "&OrderInfo=" + URLEncoder.encode(mCategoryName)+"&memName="+URLEncoder.encode(mName));

        //  webView.loadUrl(Baseurl + "payment.php?amt=" + mBalance + "&trref=" + test123 + "&OrderInfo=" + test234;

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PayNow.this, AccountDetails.class);
        startActivity(intent);
        finish();
    }
}
