/*
 * Copyright (c) 2016. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.thechandigarhgolfclub;

import android.app.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.Singleton;
import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.NotificationPojo;
import com.thechandigarhgolfclub.model.TournamentPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class Noticeboard extends Activity implements Iconstant, ConnectivityReceiveListener {
    RecyclerView recyclerView;
    ArrayList<HashMap<String, String>> mList;
    private ImageLoader imageLoader;
    private ProgressDialog progressDialog;
    private TextView title;
    private ImageView icon_app;
    FrameLayout root;
    BadgeView badgeView;
    View contentHamburger;
    private ImageView logout, notification;
    RelativeLayout toolbar;
    private static final long RIPPLE_DURATION = 250;
    private SharedPreferences sharedPreferences1;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_board);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);

        mList = new ArrayList<>();
        title = (TextView) findViewById(R.id.title);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        logout = (ImageView) findViewById(R.id.logout);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        title.setText("Notice Board");


        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Noticeboard.this, notification);
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Noticeboard.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        sharedPreferences1 = getSharedPreferences("notdel", Context.MODE_PRIVATE);
        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Noticeboard.this);
        editor = sharedPreference.edit();
        Singleton.getinstance().logout_chek(sharedPreference, logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Noticeboard.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, Notification_list.class);
                intent.putExtra("back_noti", Noticeboard.class);
                startActivity(intent);
                //   finish();
            }
        });

        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, About.class);
                startActivity(intent);
                finish();
            }
        });


        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, Course.class);
                startActivity(intent);
                finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, Club.class);
                startActivity(intent);
                finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });


        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Noticeboard.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();

        if (TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(), "notification_data"))) {
            notiBoard_json(true);
        } else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "notification_data");
            Log.e("plain", object);
            NotificationPojo mResponseObject1 = new Gson().fromJson(object, NotificationPojo.class);
            setData(mResponseObject1);
            notiBoard_json(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }

        if (!AppController.connection) {
            showSnackAlert(recyclerView, getString(R.string.dialog_message_no_internet));
        }

    }

    private void setData(NotificationPojo mResponseObject1) {
        try {
            String status = mResponseObject1.getStatus();
            Log.d("status", status);
            if (status.equals("true")) {
                mList.clear();
                for (int i = 0; i < mResponseObject1.getData().size(); i++) {
                    HashMap map = new HashMap();
                    String name = mResponseObject1.getData().get(i).getName();
                    Spanned description = Html.fromHtml(mResponseObject1.getData().get(i).getDescription());
                    //       String description = "<html><body style=text-align:justify>" + Html.toHtml(sp) + "</body></html>";
                    String image = mResponseObject1.getData().get(i).getImage();
                    map.put("name", name);
                    map.put("description", description);
                    map.put("image", image);
                    mList.add(map);
                }
                Log.d("mList", String.valueOf(mList));

                recyclerView.setAdapter(new AdapterNoticeBoard(mList));
            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("restart", "restart");
        onCreate(new Bundle());
    }

    void notiBoard_json(boolean b) {
        if (b) {
            showProgressDialog("Loading NoticeBorad");
        }
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        String url = Baseurl + "notification_board.php";
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res", response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text", plainText);
                Singleton.getinstance().saveValue(getApplicationContext(), "notification_data", plainText);
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(), "notification_data"));
                NotificationPojo mResponseObject = new Gson().fromJson(plainText, NotificationPojo.class);
                setData(mResponseObject);
                closeProgressDialog();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String object = Singleton.getinstance().getValue(getApplicationContext(), "notification_data");
                Log.e("plain", object);
                NotificationPojo mResponseObject1 = new Gson().fromJson(object, NotificationPojo.class);
                setData(mResponseObject1);
                closeProgressDialog();
            }
        });


        AppController.getInstance(Noticeboard.this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            // json_about();
            notiBoard_json(false);
            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
    }

    class AdapterNoticeBoard extends RecyclerView.Adapter<AdapterNoticeBoard.ViewHolder> {
        ArrayList<HashMap<String, String>> mList;
        private DisplayImageOptions options;

        public AdapterNoticeBoard(ArrayList<HashMap<String, String>> mList) {
            this.mList = mList;
        }

        @Override
        public AdapterNoticeBoard.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.news_events_row, parent, false);

            return new ViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(AdapterNoticeBoard.ViewHolder holder, final int position) {
            holder.name.setText(mList.get(position).get("name"));
            holder.description.setText(mList.get(position).get("description"));
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(Noticeboard.this));
            options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
            imageLoader.displayImage(mList.get(position).get("image"), holder.imageView, options);
            holder.linear_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog settingsDialog = new Dialog(Noticeboard.this, android.R.style.Theme_Holo_Dialog_NoActionBar);
                    settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.noticeboardpopup
                            , null));
                    int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
                    int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.80);
                    settingsDialog.getWindow().setLayout(width, height);
                    settingsDialog.show();
                    TextView name = (TextView) settingsDialog.findViewById(R.id.name);
                    TextView description = (TextView) settingsDialog.findViewById(R.id.description);
                    ImageView imageView = (ImageView) settingsDialog.findViewById(R.id.image_champ);
                    name.setText(mList.get(position).get("name"));
                    description.setText(mList.get(position).get("description"));
                   // imageView.setImageResource(R.drawable.image_nodata);
                   // imageView.setVisibility(View.GONE);

                    imageLoader = ImageLoader.getInstance();
                    imageLoader.init(ImageLoaderConfiguration.createDefault(Noticeboard.this));
                    options = new DisplayImageOptions.Builder().showStubImage(R.drawable.loading).showImageForEmptyUri(R.drawable.image_nodata).cacheOnDisc().cacheInMemory().build();
                    imageLoader.displayImage(mList.get(position).get("image"), imageView, options);
                }
            });

        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView name, description;
            LinearLayout linear_layout;
            //LinearLayout layImage = (LinearLayout) convertView.findViewById(R.id.layImage);
            ImageView imageView;

            public ViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.name);
                description = (TextView) itemView.findViewById(R.id.description);
                linear_layout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
                imageView = (ImageView) itemView.findViewById(R.id.image_champ);
            }
        }
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }


    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }
}
