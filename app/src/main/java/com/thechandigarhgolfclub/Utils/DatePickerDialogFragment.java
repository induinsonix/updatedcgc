package com.thechandigarhgolfclub.Utils;

/**
 * Created by prashant on 4/11/16.
 */

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;

import com.thechandigarhgolfclub.R;

import java.util.Calendar;

public class DatePickerDialogFragment extends DialogFragment implements Iconstant
{
    public static final String YEAR = "Year";
    public static final String MONTH = "Month";
    public static final String DAY = "Day";
    public static final String DATE = "Date";
    private OnDateSetListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mListener = (OnDateSetListener) activity;
    }

    @Override
    public void onDetach() {
        this.mListener = null;
        super.onDetach();
    }

    @TargetApi(11)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle b = getArguments();
        int y = b.getInt(YEAR);
        int m = b.getInt(MONTH);
        int d = b.getInt(DAY);
        int date=b.getInt(DATE);
        Calendar calendar= Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH,d);
        calendar.set(Calendar.MONTH,m);
        calendar.set(Calendar.YEAR,y);
        final DatePickerDialog picker = new DatePickerDialog(getActivity(), R.style.DialogTheme,getConstructorListener(), y, m, d);

        if (hasJellyBeanAndAbove()) {
          /*  if (date==DATE_PICKER_FROM )
            {
             */   Calendar calendar1= Calendar.getInstance();
                calendar.get(Calendar.DAY_OF_MONTH);
                calendar.get(Calendar.MONTH);
                calendar.get(Calendar.YEAR);
                picker.getDatePicker().setMaxDate(calendar1.getTimeInMillis()-1000);
            /*}
            else
            {
                picker.getDatePicker().setMaxDate(calendar.getTimeInMillis()-1000);
            }*/

            picker.setButton(DialogInterface.BUTTON_POSITIVE,
                    getActivity().getString(android.R.string.ok),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DatePicker dp = picker.getDatePicker();
                            mListener.onDateSet(dp,
                                    dp.getYear(), dp.getMonth(), dp.getDayOfMonth());
                        }
                    });
            picker.setButton(DialogInterface.BUTTON_NEGATIVE,
                    getActivity().getString(android.R.string.cancel),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    });
        }
        return picker;
    }

    private static boolean hasJellyBeanAndAbove()
    {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    private OnDateSetListener getConstructorListener() {
        return hasJellyBeanAndAbove() ? null : mListener;
    }
}


