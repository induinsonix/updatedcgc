package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.ShowMsg;
import com.thechandigarhgolfclub.Utils.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.thechandigarhgolfclub.guillotine.animation.GuillotineAnimation;
import com.thechandigarhgolfclub.model.NewsPojo;
import com.thechandigarhgolfclub.model.TournamentPojo;
import com.thechandigarhgolfclub.receiver.ConnectivityReceiveListener;

/**
 * Created by insonix on 18/10/16.
 */
public class Tournaments extends Activity implements Iconstant, ConnectivityReceiveListener {

    ArrayList tournament_dates_list;
    private static final long RIPPLE_DURATION = 250;
    RelativeLayout toolbar;
    FrameLayout root;
    ListView list_item;
    View contentHamburger;
    private TextView title;
    private ProgressDialog progressDialog;
    private ImageView icon_app;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private ImageView logout;
    private ImageView notification;
    private BadgeView badgeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upcoming_tournaments);
        if (!AppController.connection)
            AppController.getInstance(getApplicationContext()).checkConnection(this);
        AppController.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                .setLabel("Tournaments Activity")
                .build());
        tournament_dates_list = new ArrayList();
        list_item = (ListView) findViewById(R.id.list_item);
        toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = findViewById(R.id.content_hamburger);
        title = (TextView) findViewById(R.id.title);
        title.setText("Golf Calender");

        sharedPreference = PreferenceManager.getDefaultSharedPreferences(Tournaments.this);
        editor = sharedPreference.edit();
        logout = (ImageView) findViewById(R.id.logout);
        //logout
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.connection) {
                    Singleton.getinstance().logout(Tournaments.this, editor);
                } else {
                    showSnackAlert(logout, getString(R.string.dialog_message_no_internet));
                }
            }
        });

        Singleton.getinstance().logout_chek(sharedPreference, logout);
        notification = (ImageView) findViewById(R.id.notification);
        badgeView = new BadgeView(Tournaments.this, notification);
        //noti_count();
        if (AppController.connection) {
            Singleton.getinstance().noti_count(Tournaments.this, badgeView);
        } else {
            showSnackAlert(badgeView, getString(R.string.dialog_message_no_internet));
        }
        notification.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, Notification_list.class);
                startActivity(intent);
                // finish();
            }
        });


        View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about = (TextView) guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course = (TextView) guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament = (TextView) guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club = (TextView) guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery = (TextView) guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news = (TextView) guillotineMenu.findViewById(R.id.navi_news);
        icon_app = (ImageView) findViewById(R.id.icon_app);
        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, CCBActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        if(TextUtils.isEmpty(Singleton.getinstance().getValue(getApplicationContext(),"tournament_data"))) {
            json_tournaments(true);
        }
        else {
            String object = Singleton.getinstance().getValue(getApplicationContext(), "tournament_data");
            Log.e("plain",object);
            TournamentPojo mResponseObject1 = new Gson().fromJson(object, TournamentPojo.class);
            setData(mResponseObject1);
            json_tournaments(false);

            //   findViewById(R.id.rl_noData).setVisibility(View.GONE);
        }
        if (!AppController.connection) {
            showSnackAlert(list_item, getString(R.string.dialog_message_no_internet));
        }
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, About.class);
                startActivity(intent);
                finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, Course.class);
                startActivity(intent);
                finish();
            }
        });
        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, Tournaments.class);
                startActivity(intent);
                finish();
            }
        });
        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, Club.class);
                startActivity(intent);
                finish();
            }
        });
        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, Gallery.class);
                startActivity(intent);
                finish();
            }
        });
        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tournaments.this, News_events.class);
                startActivity(intent);
                finish();
            }
        });
        root.addView(guillotineMenu);
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();
    }

    private void setData(TournamentPojo mResponseObject) {
        try {
            String status = mResponseObject.getStatus();
            if (status.equalsIgnoreCase("true")) {
                tournament_dates_list.clear();
                for (int i = 0; i < mResponseObject.getData().size(); i++) {

                    String from = mResponseObject.getData().get(i).getFrom();
                    String to = mResponseObject.getData().get(i).getTo();
                    String name = mResponseObject.getData().get(i).getName();
                    tournament_dates_list.add("<b>" + name + "</b>" + "<br>From: " + from + "</br><br>To: " + to + "</br>");
                }
                Tournament_adapter adapter = new Tournament_adapter(tournament_dates_list);
                list_item.setAdapter(adapter);
            } else {
                new ShowMsg().createDialog(Tournaments.this, mResponseObject.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
           onCreate(new Bundle());
    }
    @Override
    public void onBackPressed() {
        finish();
    }


    //json of tournaments
    void json_tournaments(boolean progress_show) {
        findViewById(R.id.rl_noData).setVisibility(View.GONE);
        if(progress_show){
        showProgressDialog(getString(R.string.tournament_loading));}
        String url = Baseurl + "tournaments.php";
        Log.d("abouturl", url);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("res",response);
                String plainText = Html.fromHtml(response).toString().trim();
                Log.e("plain_text",plainText);
                Singleton.getinstance().saveValue(getApplicationContext(),"tournament_data", plainText);
                Log.e("res1", Singleton.getinstance().getValue(getApplicationContext(),"tournament_data"));
                TournamentPojo mResponseObject = new Gson().fromJson(plainText, TournamentPojo.class);
                setData(mResponseObject);
                closeProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgressDialog();
                String object = Singleton.getinstance().getValue(getApplicationContext(), "tournament_data");
                Log.e("plain",object);
                TournamentPojo mResponseObject1 = new Gson().fromJson(object, TournamentPojo.class);
                setData(mResponseObject1);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void showSnackAlert(@NonNull View view, @NonNull String message) {
        Snackbar snack = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(Color.RED);
        TextView tv = (TextView) group.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snack.show();
    }


    private void getConnection(Boolean isConnected) {
        if (isConnected) {
            json_tournaments(false);
            Singleton.getinstance().noti_count(Tournaments.this, badgeView);
            showSnackAlert(title, getString(R.string.dialog_message_internet));

        } else {
            closeProgressDialog();
            showSnackAlert(title, getString(R.string.dialog_message_no_internet));
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        getConnection(isConnected);
        AppController.connection = isConnected;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppController.getInstance(getApplicationContext()).setConnectivityListener(this);
    }

    private void showProgressDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        }
    }

    class Tournament_adapter extends BaseAdapter {
        ArrayList tournament_dates_list;

        public Tournament_adapter(ArrayList tournament_dates_list) {
            this.tournament_dates_list = tournament_dates_list;
        }

        @Override
        public int getCount() {
            return tournament_dates_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(Tournaments.this);
            convertView = inflater.inflate(R.layout.row_singletournament, null);
            TextView tournament_date = (TextView) convertView.findViewById(R.id.tournament_date);
            tournament_date.setText(Html.fromHtml(tournament_dates_list.get(position) + ""));
            return convertView;
        }
    }


}
