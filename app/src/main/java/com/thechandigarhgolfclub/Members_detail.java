package com.thechandigarhgolfclub;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.thechandigarhgolfclub.Utils.AppController;
import com.thechandigarhgolfclub.Utils.Iconstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by insonix on 2/11/16.
 */
public class Members_detail extends Activity implements Iconstant {

LinearLayout HomePage;
//  FrameLayout root;
    private SharedPreferences sharedPreference;
    private ProgressDialog progressDialog;
TextView first_nm,phone,father_nm,email,dob,address1,creditlimit,state,city,mobile;
  //  private RelativeLayout toolbar;

    private TextView title;

    private ArrayList list_tooltip;
    TextView back_text;
//ListView list_item;
 //   ImageView ibLoginButton;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.members_detail);
      //  tooltip=(ImageView)findViewById(R.id.tooltip);
       // popupWindow.showAtLocation(this.findViewById(R.id.tooltip), Gravity.CENTER, 0, 0);

        HomePage=(LinearLayout)findViewById(R.id.HomePage);
   //     ledger_lay=(LinearLayout)findViewById(R.id.ledger_lay);
      //  root=(FrameLayout)findViewById(R.id.root);
       // memcode=(TextView)findViewById(R.id.memcode);
       // list_item=(ListView)findViewById(R.id.list_item);
       // category_nm=(TextView)findViewById(R.id.category_nm);
        first_nm=(TextView)findViewById(R.id.first_nm);
        back_text=(TextView)findViewById(R.id.back_text);
        phone=(TextView)findViewById(R.id.phone);
        father_nm=(TextView)findViewById(R.id.father_nm);
        email=(TextView)findViewById(R.id.email);
       dob=(TextView)findViewById(R.id.dob);
        address1=(TextView)findViewById(R.id.address1);
        creditlimit=(TextView)findViewById(R.id.creditlimit);
     //   membalance=(TextView)findViewById(R.id.membalance);
        state=(TextView)findViewById(R.id.state);
        city=(TextView)findViewById(R.id.city);
        mobile=(TextView)findViewById(R.id.mobile);



        address1.setText(getIntent().getStringExtra("address"));
       father_nm.setText(getIntent().getStringExtra("FatherName"));
        dob.setText(getIntent().getStringExtra("BirthDate"));
        email.setText(getIntent().getStringExtra("EmailID"));
        city.setText(getIntent().getStringExtra("CityName"));
        first_nm.setText(getIntent().getStringExtra("Name"));
        mobile.setText(getIntent().getStringExtra("MobileNo"));
     //   ibLoginButton=(ImageView)findViewById(R.id.ibLoginButton);



      //  ibLoginButton.setImageResource(R.drawable.golf);



   /*     View guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        TextView navi_about=(TextView)guillotineMenu.findViewById(R.id.navi_about);
        TextView navi_course=(TextView)guillotineMenu.findViewById(R.id.navi_course);
        TextView navi_tournament=(TextView)guillotineMenu.findViewById(R.id.navi_tournament);
        TextView navi_club=(TextView)guillotineMenu.findViewById(R.id.navi_club);
        TextView navi_gallery=(TextView)guillotineMenu.findViewById(R.id.navi_gallery);
        TextView navi_news=(TextView)guillotineMenu.findViewById(R.id.navi_news);
        navi_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Members_detail.this, About.class);
                startActivity(intent);
               // finish();
            }
        });
        navi_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Members_detail.this,Course.class);
                startActivity(intent);
              //  finish();
            }
        });

        navi_tournament.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Members_detail.this,Tournaments.class);
                startActivity(intent);
                //finish();
            }
        });

        navi_club.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Members_detail.this,Club.class);
                startActivity(intent);
               // finish();
            }
        });


        navi_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Members_detail.this,Gallery.class);
                startActivity(intent);
                //finish();
            }
        });




        navi_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Members_detail.this, News_events.class);
                startActivity(intent);
                // finish();
            }
        });
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();



        icon_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Members_detail.this, CCBActivity.class);
                startActivity(intent);
                finish();

            }
        });
*/

        progressDialog=new ProgressDialog(Members_detail.this);
        sharedPreference= PreferenceManager.getDefaultSharedPreferences(Members_detail.this);

        //Tooltip

      /*  popupWindow=new PopupWindow(Members_detail.this);
        LayoutInflater inflater=LayoutInflater.from(Members_detail.this);
        final View contentView=inflater.inflate(R.layout.popup_example, null, false);
        popupWindow = new PopupWindow(contentView, FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT, true);

        Drawable d = new ColorDrawable(Color.BLACK);
        d.setAlpha(130);
        popupWindow.setBackgroundDrawable(d);
        popupWindow.setFocusable(true);
        popupWindow.setTouchable(true);

        facility_detail=(TextView)contentView.findViewById(R.id.tooltip_text);
        member_login=(TextView)contentView.findViewById(R.id.member_login);
        ledger_detail=(TextView)contentView.findViewById(R.id.ledger_detail);
        facility_detail.setText("Facility Details");
        member_login.setText("Member's Details");
        ledger_detail.setText("Ledger Details");

        facility_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (Singleton.getinstance().isNetworkAvailable(Members_detail.this)){
                    json_facility();
                }
                else {
                    Singleton.getinstance().dialog_internetconnection(Members_detail.this);
                }
//                Intent intent = new Intent(Members_detail.this, Members_detail.class);
//                overridePendingTransition(0, 0);
//                intent.putExtra("chek", "1");
//                startActivity(intent);
//                finish();
            }
        });
        member_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(Members_detail.this, Members_detail.class);
                overridePendingTransition(0, 0);

                startActivity(intent);
                finish();
            }
        });*/
/*

        ledger_detail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String url=Baseurl_member+"MembersLedger?memcode="+sharedPreference.getString("MemCode","");
                popupWindow.dismiss();
                if (Singleton.getinstance().isNetworkAvailable(Members_detail.this)){
                    json_ledger(url);
                }
                else {
                    Singleton.getinstance().dialog_internetconnection(Members_detail.this);
                }
            }
        });

        date_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

                SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
                Date sdate = null,edate = null;

                try {
                    sdate = formate.parse(start_date.getText().toString());
                          edate = formate.parse(end_date.getText().toString());

                } catch (ParseException e) {


                }
                String url=Baseurl_member+"MembersLedger?memcode="+sharedPreference.getString("MemCode","")+"&sdate="+formate.format(sdate)+"&edate="+formate.format(edate);
                if (Singleton.getinstance().isNetworkAvailable(Members_detail.this)){
                    json_ledger(url);
                }
                else {
                    Singleton.getinstance().dialog_internetconnection(Members_detail.this);
                }


            }
        });
*/





//        tooltip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.showAsDropDown(tooltip);
//            }
//        });
        /*if (getIntent().getStringExtra("chek").equals("1")){

            if (Singleton.getinstance().isNetworkAvailable(Members_detail.this)){
                json_tooltil();
            }
            else {
                Singleton.getinstance().dialog_internetconnection(Members_detail.this);
            }

        }
        else{

        }*/




//        if (Singleton.getinstance().isNetworkAvailable(Members_detail.this)){
//            json_memdetail();
//        }
//        else {
//            Singleton.getinstance().dialog_internetconnection(Members_detail.this);
//        }

//        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // TODO Auto-generated method stub
//                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
//                    popupWindow.dismiss();
//                }
//                return true;
//            }
//        });


        back_text.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Members_detail.this, AccountDetails.class);
                startActivity(intent);
                finish();
            }
        });

    }








    public static Rect locateView(View v)
    {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try
        {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe)
        {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Members_detail.this, AccountDetails.class);
        startActivity(intent);
        finish();
    }






    // json of facility

    void json_facility(){
        HomePage.setVisibility(View.GONE);
        //ibLoginButton.setVisibility(View.GONE);

        progressDialog.setMessage("Loading..");
        progressDialog.show();
        String url=Baseurl_member+"GetFacilityDetails?memcode="+sharedPreference.getString("MemCode","");
        list_tooltip=new ArrayList();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                try {
                    Boolean status=response.getBoolean("Status");
                    String ErrMsg=response.getString("ErrMsg");
                    Log.d("status", status.toString());
                    if (status.equals(true))
                    {
                        JSONArray jsonArray=response.getJSONArray("FacDetails");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject=jsonArray.getJSONObject(i);
                            list_tooltip.add(jsonObject.getString("FacilityName"));
                        }
                        Member_adapter pop_adapter=new Member_adapter(list_tooltip);
                      //  list_item.setAdapter(pop_adapter);
                       // list_item.setVisibility(View.VISIBLE);
                        HomePage.setVisibility(View.GONE);
                        //ibLoginButton.setVisibility(View.GONE);
                        title.setText("Facility Detail");

                    }
                    else{
                        Toast.makeText(Members_detail.this, "" + ErrMsg, Toast.LENGTH_LONG).show();
                    }




                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                progressDialog.dismiss();
                Toast.makeText(Members_detail.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
   }

//json of memberbalance

    void json_membal(){
        progressDialog.setMessage("Loading..");
        progressDialog.setCancelable(true);
        progressDialog.show();


        String url=Baseurl_member+"GetMemberBalance?memcode="+sharedPreference.getString("MemCode","");
        list_tooltip=new ArrayList();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("res", response.toString());
                try {
                    Boolean status=response.getBoolean("Status");
                    String ErrMsg=response.getString("ErrMsg");
                    Log.d("status", status.toString());
                    if (status.equals(true))
                    {
                        String Balance=response.getString("Balance");
                        //membalance.setText(Balance);
                    }
                    else{
                        Toast.makeText(Members_detail.this, "" + ErrMsg, Toast.LENGTH_LONG).show();
                    }




                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("errorss", error.toString());
                progressDialog.dismiss();
                Toast.makeText(Members_detail.this, "Slow Internet Connection", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);

    }








    class Member_adapter extends BaseAdapter {
        ArrayList list_tooltip;

        public Member_adapter(ArrayList list_tooltip) {
            this.list_tooltip=list_tooltip;
        }

        @Override
        public int getCount() {
            return list_tooltip.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater= LayoutInflater.from(Members_detail.this);
            convertView=inflater.inflate(R.layout.facility_row,null);
            TextView tournament_date=(TextView)convertView.findViewById(R.id.tournament_date);
            tournament_date.setText(list_tooltip.get(position)+"");



            return convertView;
        }
    }


}
