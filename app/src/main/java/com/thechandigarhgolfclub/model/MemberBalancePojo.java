package com.thechandigarhgolfclub.model;

/**
 * Created by root on 13/1/17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberBalancePojo {

    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("ErrMsg")
    @Expose
    private String errMsg;
    @SerializedName("Balance")
    @Expose
    private Float balance;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

}