package com.thechandigarhgolfclub.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.thechandigarhgolfclub.Utils.Iconstant;
import com.thechandigarhgolfclub.Utils.MySharedPreferences;


/**
 * Created by prashant on 17/10/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService implements Iconstant {

    private static final String TAG = "MyFirebaseIIDService";
    private String url;
    private String UId;
    private MySharedPreferences mySharedPreference;
    public static String refreshedToken;
    public SharedPreferences sharedPreferences,sharedPreferences1;	;
    private SharedPreferences.Editor editor,editor1;

    @Override
    public void onTokenRefresh() {
        Log.d("chek token", "tokennn");
        sharedPreferences=getSharedPreferences("notdel", Context.MODE_PRIVATE);
        sharedPreferences1=getSharedPreferences("neverdel", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor1=sharedPreferences1.edit();
        mySharedPreference = new MySharedPreferences(getApplicationContext());
        //Getting registration token
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        editor.putString("tokenn",refreshedToken);
        editor.commit();


        editor1.putString("tokenn",refreshedToken);
        editor1.commit();

        //Displaying token in logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
       // sendRegistrationToServer(refreshedToken);
        mySharedPreference.editor_put("token",refreshedToken);
        mySharedPreference.saveNotificationSubscription(true,refreshedToken);
    }

}




